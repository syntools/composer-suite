#ifndef MAL_COMPOSERS_H
#define MAL_COMPOSERS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <zlib.h>
#include "minimap.h"
#include "kseq.h"
#include "mmpriv.h"

#define  SRC_REF_ARG 1
#define  SEQ_ID_ARG 2
#define  LO_ARG 3
#define  HI_ARG 4
#define  TARGET_REF_1_ARG 5
#define  _PATH_MAX 4096
#define  _MULTI_LOCUS 0  // this tells the program to expect multiple sequences to analyze
#define  _CONFIG_SIZE_MAX 1024 * 32
#define  _CALL_STR_MAX 4096
#define  _LABEL_MAX 1024
#define  _EOS -1
#define  _SORTED 1
#define  _UNSORTED 0
#define  _APPEND 1
#define  _REPLACE 2
#define  _READ_ONLY 3
#define  _EXTRACT_MODE_ON 1
#define  _EXTRACT_MODE_OFF 0
#define _STR_POOL_MAX 64
#define  analysis_mode_none  "none"
#define  analysis_mode_ava_ont "ava-ont"
#define  analysis_mode_acr "sr"
#define  analysis_mode_sr "sr"
#define  analysis_mode_cds "splice"
#define  analysis_mode_pb "map10k"
#define  analysis_mode_asm5 "asm" 
#define  analysis_mode_asm10 "asm5"
#define  analysis_mode_asm20 "asm10"
#define  analysis_mode_repel "splice"
#define  analysis_mode_splice "splice"
#define  analysis_mode_splice_hq "splice:hq"
#define  analysis_mode_exact "exact" 
#define  analysis_mode_hapblock "asm5"
#define  analysis_mode_gdna "asm5"

#define  _ROI_DEFAULT_FILE_PATH "./.barber/barber_roi.fa"
#define  _EXTRACTED_DEFAULT_FILE_PATH "./barber_extracted.fa"

#define key_job "job"
#define key_threads "threads"     
#define key_repel_file "repel_file"  
#define key_cds_file "cds_file"    
#define key_roi_ref_file "roi_ref_file"
#define key_ref_file "ref_file"    
#define key_mpas_file_path "mpas_file_path"    
#define key_createfile_path "createfile_path_path"    
#define key_username "username"    
#define key_mode "mode"        
#define key_roi_seqid "roi_seqid"  
#define key_roi_lo "roi_lo"      
#define key_roi_hi "roi_hi"      
#define key_fivep_extra "fivep_extra" 
#define key_threep_extra "threep_extra"
#define key_roi_block_size "roi_block_size"
#define key_query_file "query_file"  
#define key_results_file "results_file"
#define key_roi_file "roi_file"    
#define key_keep_roi_file "keep_roi_file"
#define key_extract_mode "extract_mode"
#define key_quiet_mode "quiet_mode"
#define key_report_introns "report_introns"
#define key_flush "flush"
#define key_max_edit_dist "max_edit_distance"
#define key_report_type "report_type"
#define key_show_bed "show_bed"
#define key_show_cigar "show_cigar"
#define key_show_rcigar "show_rcigar"
#define key_show_sampaf "show_sampaf"
#define key_show_sortid "show_sortid"
#define key_show_gcplot "show_gcplot"
#define key_show_onehot "show_onehot"
#define key_ngap_threshold "ngap_threshold"

#define value_report_type_paf "paf"
#define value_report_type_bed "bed"
#define value_report_type_mpas "mpas"
#define value_extract_mode_none "none"
#define value_extract_mode_gene "gene_mpas"
#define value_extract_mode_gene_model "gene_model"
#define value_extract_mode_promoter "promoter_fasta"
#define const_report_type_paf 0
#define const_report_type_bed 1
#define const_report_type_mpas 2

#define extract_mode_none 0
#define extract_mode_alignment 1
#define extract_mode_alignment_extra 2
#define extract_mode_extra 3

#define CMD_OPEN "OPEN"
#define CMD_CLOSE "CLOSE"
#define CMD_DATABASE "DATABASE"
#define CMD_PRINT "PRINTTO"
#define CMD_INDEX "INDEX"
#define CMD_ALIGN "ALIGN"
#define CMD_ECHO "ECHO"
#define CMD_ANALYZE "ANALYZE"
#define CMD_VALUE_REPEL "REPEL"
#define CMD_VALUE_CDS "CDS"
#define CMD_VALUE_CURRENT_REF "CURRENT_REF"
#define CMD_VALUE_ROI "ROI"
#define CMD_VALUE_COUNT "COUNT"
#define CMD_VALUE_QUERY_FILE "QUERY_FILE"
#define CMD_VALUE_EXTRACT_HIT "EXTRACT_HIT"
#define CMD_VALUE_DATABASE_CREATEMPAS "CREATEMPAS"
#define CMD_VALUE_DATABASE_CLOSEMPAS "CLOSEMPAS"
#define CMD_VALUE_NGAP "NGAP"
#define CMD_VALUE_SETTINGS "SETTINGS"
typedef struct {
  char *config_path;
  char *jobname;
  char *repel_path;
  char *cds_path;
  char *query_path;
  char *results_path;
  char *username;
  char *analysis_mode;
  char *roi_ref_path;
  char *roi_file_path;   // = _ROI_FILE_PATH;
  char *seqid;
  char *current_ref_path;
  char *report_type;
  char *mpas_file_path;
  char *current_ref_name;
  uint64_t locoord;
  uint64_t hicoord;
  uint64_t fivep_extra;
  uint64_t threep_extra;
  uint64_t roi_block_size;
  int keep_roi_file;
  int report_introns;
  int extract_mode;
  int quiet_mode;
  int max_edit_distance;
  int show_sampaf;
  int show_bed;
  int show_rcigar;
  int show_cigar;
  int show_sortid;
  int show_gcplot;
  int show_onehot;
  int show_progress;
  int step;
  int n_threads;
  int flush;
  int ngap_threshold;
  int gzip_mpas;
  mm_idx_reader_t *repel_mm_reader;
  mm_idx_reader_t *cds_mm_reader;
  mm_idx_reader_t *current_mm_reader;
  mm_idx_t *repel_mm_idx;
  mm_idx_t *cds_mm_idx;
  mm_idx_t *current_mm_idx;
  mm_idxopt_t repel_idx_opt;
  mm_idxopt_t cds_idx_opt;
  mm_idxopt_t current_idx_opt;
  mm_mapopt_t repel_map_opt;
  mm_mapopt_t cds_map_opt;
  mm_mapopt_t current_map_opt;
  char *str_pool[_STR_POOL_MAX];
  unsigned long str_pool_sz[_STR_POOL_MAX];
  int progidx;
  int allocations;
  int alignment_index;
  int last_aln_idx;
  unsigned long current_global_hitid;
  FILE *store_file;
} Globals;
#define HITID_STR_MAX 64
typedef struct {
   char *chromosome;
   uint64_t locoord;
   uint64_t hicoord;
   int strand;
} GeneCoord;

GeneCoord **gene_locations;

typedef struct {
    float sortf;
    unsigned long ed;
    char hitid[HITID_STR_MAX];
    uint64_t fivep;
    uint64_t threep;
    char *hitseq;    // this is the hit conting!
    char *pattern;
} Alignment;

Alignment **_alignments;

int align (char *instruction);


#ifdef __cplusplus
}
#endif

#endif
