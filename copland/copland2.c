
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <math.h>
#include <zlib.h>
#include <pthread.h>
#include "minimap.h"
#include "kseq.h"
#include "mmpriv.h"
#include "composers.h"
#include <uuid/uuid.h>
#include <libgen.h>
#define POOL_STR_INIT 1
#define POOL_STR_KEEP 0
#define POOL_CSV_DATA 2
#define POOL_NEWSTDOUT_BUF 3
#define SP_BLOCK_SIZE 102400
#define SHOW_ALIGNMENT 1
#define DO_PRINTF_STDERR 1
KSEQ_INIT(gzFile, gzread)

int num_refs = 0;
FILE *outfile;
Globals *cs_globals;
pthread_mutex_t lock;
// static inline int str_empty (const char *str) { return (str[0] == 0); }
static inline int str_equals (const char *str1, const char *str2) { return (strcmp(str1, str2) == 0); }
static inline char *new_zero_str(unsigned long size) { char *ns = (char *) malloc(size + 1); assert(ns); ns[0] = 0;  return ns; } 
static inline void free_mem (void *ptr) { if (ptr) { free (ptr);  }}
void *malloc_mem(uint64_t size) { void *p = malloc(size); if (p)  return p; else return NULL; }
void chop_off_newline(char *s) { register size_t i; register size_t l = strlen(s); for(i=0;i<l;i++) if (s[i] == '\n') { s[i] = 0; return; } }
#define _SZ 32769
char *string_pool (unsigned long size, int init, int caller_id) {
  char* this_str = cs_globals->str_pool[caller_id];
  unsigned long this_sz = cs_globals->str_pool_sz[caller_id];
  pthread_mutex_lock(&lock);
  if (this_sz == 0 && size > 0) {
     if (cs_globals->str_pool[caller_id] != NULL)
        free (cs_globals->str_pool[caller_id]);

     cs_globals->str_pool[caller_id] = new_zero_str(size);
     cs_globals->str_pool_sz[caller_id] = size;
     return cs_globals->str_pool[caller_id];
  }
  if (this_str && (this_sz >= size)) {
     if (init)
        this_str[0] = 0;
     return this_str;
  }
  if (this_str && (this_sz < size)) {
     char *newstr =  realloc(cs_globals->str_pool[caller_id], size);
     if (newstr) {
        cs_globals->str_pool[caller_id] = newstr;
        cs_globals->str_pool_sz[caller_id] = size;
        return newstr;
     } else {
        return NULL;
     }
  }
  pthread_mutex_unlock(&lock);
  return NULL;
}
void safe_strcat(char* dest, const char* src, size_t src_size) {
    pthread_mutex_lock(&lock);
    // Assume dest_size is the total size of the destination buffer.
    strlcat(dest, src, src_size); // Leave space for the null terminator
    pthread_mutex_unlock(&lock);
}
void safe_strcpy(char* dest, const char* src, size_t dest_size) {
    pthread_mutex_lock(&lock);
    // Assume dest_size is the total size of the destination buffer.
    strncpy(dest, src, dest_size); // Leave space for the null terminator
    dest[dest_size] = '\0';        // Ensure null termination
    pthread_mutex_unlock(&lock);
}
void sp_cat (const char *str, unsigned long len, int caller_id) {
  char* this_str = cs_globals->str_pool[caller_id];
  unsigned long this_sz = cs_globals->str_pool_sz[caller_id];
  unsigned long new_size = this_sz + len;
  if (new_size < SP_BLOCK_SIZE)
      new_size = this_sz + SP_BLOCK_SIZE;
  if (this_str && (this_sz < new_size)) {
     char *newstr =  realloc(cs_globals->str_pool[caller_id], new_size);
     if (newstr) {
        cs_globals->str_pool[caller_id] = newstr;
        cs_globals->str_pool_sz[caller_id] = new_size;
     }
    safe_strcat (newstr, str, len); 
  }
}
void chr_from_roi_name(const char *roi_name, char *chr) {
   int i;
   int len = strlen(roi_name);
   for (i=0;i<len;i++) {
      if (roi_name[i] != '_') {
         chr[i] = roi_name[i];
      } else {
         chr[i] = '\0';
      }
   }
}
FILE *cs_fopen(const char * restrict path, int _MODE) {
   if (_MODE == _APPEND) return fopen(path,"a");
   if (_MODE == _REPLACE) return fopen(path,"w");
   if (_MODE == _READ_ONLY) return fopen(path,"r");
   return NULL;
   // this is a wrapper for future file locking code
}

int value_as_int (const char *value) {
   if (str_equals(value, "on")) return 1;
   if (str_equals(value, "yes")) return 1;
   if (str_equals(value, "1")) return 1;
   if (str_equals(value, "true")) return 1;
   if (str_equals(value, "off")) return 0;
   if (str_equals(value, "no")) return 0;
   if (str_equals(value, "0")) return 0;
   if (str_equals(value, "false")) return 0;
   return 0;
}

// Initialize the mutex before using safe_strcpy in a multithreaded context

void informative_abort(const char *format, const char *info1, const char *info2) {
      fprintf (stderr, format, info1, info2);
      fprintf (outfile, format, info1, info2);
      fflush(outfile);
      exit(0); 
}
int file_exists (const char *path) { 
   if (access(path, F_OK) != 0) {
      informative_abort("File %s does not exist [%s]\n",path, "");
      return 0;
   } else return 1; 
}
#define UUID_CALLER 10
char *uuid_pool (void) {
    uuid_t uuid;
    char *uuid_str  = string_pool (37, 1, UUID_CALLER);
    // Generate a UUID
    uuid_generate(uuid);

    // Convert the binary UUID to a string
    uuid_unparse_lower(uuid, uuid_str);
    return uuid_str;
}
char *timestamp (int _long) {
    char ts[PATH_MAX];
    ts[0] = 0;
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    if (_long) 
      sprintf(ts, "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    else 
      sprintf(ts, "%d-%02d-%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
    return strdup(ts);
}
int set_mm_opt_by_analysis_mode(char  *mode, mm_idxopt_t *io, mm_mapopt_t *mo) {
    if (str_equals(mode,analysis_mode_none)) {
        // if (io) mm_idxopt_init_cs (io);
        // if (mo) mm_mapopt_init_cs (mo);
    } else if (str_equals(mode, analysis_mode_ava_ont)) {
        if (io) io->flag = 0;
        if (io) io->k = 15;
        if (io) io->w = 5;
        if (mo) mo->flag |= MM_F_ALL_CHAINS | MM_F_NO_DIAG | MM_F_NO_DUAL | MM_F_NO_LJOIN | MM_F_CIGAR | MM_F_OUT_MD;
        if (mo) mo->min_chain_score = 100; if (mo) mo->pri_ratio = 0.0f; if (mo) mo->max_gap = 10000; if (mo) mo->max_chain_skip = 25;
        if (mo) mo->bw = 2000;
    } else if (str_equals(mode,  analysis_mode_asm5) || str_equals(mode, analysis_mode_hapblock)) {
        if (io) io->flag = 0;
        if (io) io->k = 19;
        if (io) io->w = 19;
        if (mo) mo->a = 1;
        if (mo) mo->b = 19;
        if (mo) mo->q = 39;
        if (mo) mo->q2 = 81;
        if (mo) mo->e = 3;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 200;
        if (mo) mo->min_mid_occ = 100;
        if (mo) mo->min_dp_max = 200;
        if (mo) mo->best_n = 50;
    } else if (str_equals(mode, analysis_mode_asm10)) {
        if (io) io->flag = 0;
        if (io) io->k = 19;
        if (io) io->w = 19;
        if (mo) mo->a = 1;
        if (mo) mo->b = 9;
        if (mo) mo->q = 16;
        if (mo) mo->q2 = 41;
        if (mo) mo->e = 2;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 200;
        if (mo) mo->min_mid_occ = 100;
        if (mo) mo->min_dp_max = 200;
        if (mo) mo->best_n = 50;
    } else if (str_equals(mode,analysis_mode_asm20)) {
        if (io) io->flag = 0;
        if (io) io->k = 19;
        if (io) io->w = 10;
        if (mo) mo->a = 1;
        if (mo) mo->b = 4;
        if (mo) mo->q = 6;
        if (mo) mo->q2 = 26;
        if (mo) mo->e = 2;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 200;
        if (mo) mo->min_mid_occ = 100;
        if (mo) mo->min_dp_max = 200;
        if (mo) mo->best_n = 50;
    } else if (str_equals(mode,analysis_mode_acr)
               || str_equals( mode , analysis_mode_sr)
               ) {
        if (io) io->flag = 0;
        if (io) io->k = 13;
        if (io) io->w = 8;
//        if (io) io->k = 15;
//        if (io) io->w = 5;
        if (mo) mo->flag |= MM_F_SR
                | MM_F_FRAG_MODE
                | MM_F_2_IO_THREADS
                | MM_F_HEAP_SORT
                | MM_F_OUT_CS
                | MM_F_CIGAR
                | MM_F_OUT_MD;
        if (mo) mo->pe_ori = 0<<1|1; // FR
        if (mo) mo->a = 2;
        if (mo) mo->b = 8;
        if (mo) mo->q = 12;
        if (mo) mo->e = 2;
        if (mo) mo->q2 = 24;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 100;
        if (mo) mo->end_bonus = 10;
        if (mo) mo->max_frag_len = 800;
        if (mo) mo->max_gap = 100;
        if (mo) mo->bw = 100;
        if (mo) mo->pri_ratio = 0.5f;
        if (mo) mo->min_cnt = 2;
        if (mo) mo->min_chain_score = 25;
        if (mo) mo->min_dp_max = 40;
        if (mo) mo->best_n = 20;
        if (mo) mo->mid_occ = 1000;
        if (mo) mo->max_occ = 5000;
        if (io) io->bucket_bits = 14;
        if (io) io->batch_size = 4000000000ULL;
        if (mo) mo->mini_batch_size = 50000000;
        // if (str_equals(mode, analysis_mode_skim_map))
        //     if (mo) mo->sc_ambi = 1;
    } else if (str_equals( mode, analysis_mode_cds) 
               || str_equals(mode, analysis_mode_splice)
               || str_equals(mode, analysis_mode_splice_hq)) {
        if (io) io->flag = 0;
        if (io) io->k = 15;
        if (io) io->w = 5;
        if (mo) mo->flag |= MM_F_SPLICE
                        | MM_F_SPLICE_FOR
                        | MM_F_SPLICE_REV
                        | MM_F_SPLICE_FLANK
                        | MM_F_CIGAR
                        | MM_F_OUT_MD;
        if (mo) mo->max_gap = 2000;
        if (mo) mo->max_gap_ref = mo->bw = 200000;
        if (mo) mo->a = 1;      //match score
        if (mo) mo->b = 2;      // mismatch score
        if (mo) mo->q = 2;
        if (mo) mo->e = 1;
        if (mo) mo->q2 = 32;
        if (mo) mo->e2 = 0;
        if (mo) mo->noncan = 9;
        if (mo) mo->junc_bonus = 9;
        if (mo) mo->zdrop = 200;
        if (mo) mo->zdrop_inv = 100; // because mo->a is halved
        if (str_equals(mode, analysis_mode_splice_hq)) {
            if (mo) mo->junc_bonus = 5;
            if (mo) mo->b = 4;
            if (mo) mo->q = 6;
            if (mo) mo->q2 = 24;
        }
    } else return -1;
    return 0;
}
void setup ( ) {
   int i;
   for (i=0;i<_STR_POOL_MAX;i++) {
      cs_globals->str_pool[i] = NULL;
      cs_globals->str_pool_sz[i] = 0;
   }
   cs_globals->config_path = new_zero_str (_CALL_STR_MAX);
   cs_globals->jobname = new_zero_str (_LABEL_MAX);
   cs_globals->repel_path = new_zero_str (_PATH_MAX);
   cs_globals->cds_path = new_zero_str (_PATH_MAX);
   cs_globals->query_path = new_zero_str (_PATH_MAX);
   cs_globals->results_path = new_zero_str (_PATH_MAX);
   cs_globals->username = new_zero_str (_LABEL_MAX);
   cs_globals->analysis_mode = new_zero_str (_LABEL_MAX);
   cs_globals->roi_ref_path = new_zero_str (_PATH_MAX);
   cs_globals->roi_file_path = new_zero_str (_PATH_MAX);   // = _ROI_FILE_PATH;
   cs_globals->mpas_file_path = new_zero_str (_PATH_MAX);   // = _ROI_FILE_PATH;
   cs_globals->seqid = new_zero_str (_LABEL_MAX);
   cs_globals->current_ref_path = new_zero_str (_PATH_MAX);
   cs_globals->current_ref_name = new_zero_str (_PATH_MAX);
   cs_globals->report_type = new_zero_str (_LABEL_MAX);
   strncpy(cs_globals->report_type, value_report_type_paf, strlen(value_report_type_paf));
   sprintf(cs_globals->jobname, "%d", getpid());
   cs_globals->n_threads = 8;
   cs_globals->flush = 0;
   cs_globals->max_edit_distance = 20;
   strncpy(cs_globals->analysis_mode, analysis_mode_gdna,strlen(analysis_mode_gdna));
   strncpy(cs_globals->roi_file_path, _ROI_DEFAULT_FILE_PATH, PATH_MAX);
   cs_globals->locoord = 0;
   cs_globals->hicoord = 0;
   cs_globals->extract_mode = extract_mode_extra;
   cs_globals->quiet_mode = 1;
   cs_globals->fivep_extra = 0;
   cs_globals->threep_extra = 0;
   cs_globals->roi_block_size = 0;
   cs_globals->step = 0;
   cs_globals->repel_mm_reader = NULL;
   cs_globals->cds_mm_reader = NULL;
   cs_globals->current_mm_reader = NULL;
   cs_globals->repel_mm_idx = NULL;
   cs_globals->cds_mm_idx = NULL;
   cs_globals->current_mm_idx = NULL;
   cs_globals->keep_roi_file = 0;
   cs_globals->show_bed = 1;
   cs_globals->show_rcigar = 1;
   cs_globals->show_cigar = 0;
   cs_globals->show_sampaf = 0;
   cs_globals->show_sortid = 0;
   cs_globals->show_gcplot = 0;
   cs_globals->show_onehot = 0;
   _alignments = (Alignment **) malloc_mem(sizeof(_alignments));
   assert (_alignments);
   cs_globals->last_aln_idx = 0;
   cs_globals->ngap_threshold = 0;
   // cs_globals->store_file = stdout;
   cs_globals->show_progress = 10000;
   cs_globals->gzip_mpas = 0;

//    char *newstdout = string_pool(sizeof(uint8_t) * (32 * 1024), 0, POOL_NEWSTDOUT_BUF);
//    setvbuf(stdout, newstdout, _IOLBF, sizeof(uint8_t) * (32 * 1024));
//   char *csv_data = string_pool(10240, 1, POOL_CSV_DATA);
}
void breakdown () {
   free_mem(cs_globals->mpas_file_path);
   free_mem(cs_globals->config_path);
   free_mem(cs_globals->jobname);
   free_mem(cs_globals->repel_path);
   free_mem(cs_globals->cds_path);
   free_mem(cs_globals->query_path);
   free_mem(cs_globals->results_path);
   free_mem(cs_globals->username);
   free_mem(cs_globals->analysis_mode);
   free_mem(cs_globals->roi_ref_path);
   free_mem(cs_globals->roi_file_path);   // = _ROI_FILE_PATH;
   free_mem(cs_globals->seqid);
   free_mem(cs_globals->current_ref_path);
   free_mem(cs_globals->current_ref_name);
   free_mem(cs_globals->report_type);
   int i;
   for (i=0;i<cs_globals->last_aln_idx;i++) {
      if (_alignments[i]->pattern) {
         free_mem (_alignments[i]->pattern);
      }
      if (_alignments[i]->hitseq) {
         free_mem (_alignments[i]->hitseq);
      }
      free_mem (_alignments[i]);
   }
   // fclose (stdout);
   
   for (i=0;i<_STR_POOL_MAX;i++) {
      if (cs_globals->str_pool[i])
         free(cs_globals->str_pool[i]);
   }
   // delete the roi file
   // free up memory if a ref is open
   // free up memory if a roi ref is open
   // free any malloc's not already freed
}
void process_kvp (char *key, char *value) {
   if (key[0] > 96 && key[0] < 123) {
      if (str_equals(key, key_job))           { strncpy(cs_globals->jobname, value, PATH_MAX);                return; }
      if (str_equals(key, key_threads))       { cs_globals->n_threads = atoi(value);               return; }
      if (str_equals(key, key_repel_file))    { if (file_exists(value))                strncpy(cs_globals->repel_path, value, PATH_MAX);       return; } 
      if (str_equals(key, key_cds_file))      { if (file_exists(value))                strncpy(cs_globals->cds_path, value, PATH_MAX);         return; }
      if (str_equals(key, key_roi_ref_file))  { if (file_exists(value))                strncpy(cs_globals->roi_ref_path, value, PATH_MAX);     return; }
      if (str_equals(key, key_ref_file))      { if (file_exists(value))                strncpy(cs_globals->current_ref_path, value, PATH_MAX); return; }
      if (str_equals(key, key_username))      { strncpy(cs_globals->username, value, PATH_MAX);               return; }
      if (str_equals(key, key_mode))          { strncpy(cs_globals->analysis_mode, value, PATH_MAX);          return; }
      if (str_equals(key, key_roi_seqid))     { strncpy(cs_globals->seqid, value, PATH_MAX);                  return; }
      if (str_equals(key, key_roi_lo))        { cs_globals->locoord = atoi(value);                 return; }
      if (str_equals(key, key_roi_hi))        { cs_globals->hicoord = atoi(value);                 return; }
      if (str_equals(key, key_fivep_extra))   { cs_globals->fivep_extra = atoi(value);             return; }
      if (str_equals(key, key_threep_extra))  { cs_globals->threep_extra = atoi(value);            return; }
      if (str_equals(key, key_roi_block_size)){ cs_globals->roi_block_size = atoi(value);          return; }
      if (str_equals(key, key_query_file))    { strncpy(cs_globals->query_path, value, PATH_MAX);             return; }
      if (str_equals(key, key_results_file))  { strncpy(cs_globals->results_path, value, PATH_MAX);           return; }
      if (str_equals(key, key_mpas_file_path)){ strncpy(cs_globals->mpas_file_path, value, PATH_MAX);         return; }
      if (str_equals(key, key_roi_file))      { strncpy(cs_globals->roi_file_path, value, PATH_MAX);          return; }
      if (str_equals(key, key_keep_roi_file)) { cs_globals->keep_roi_file = value_as_int(value);   return; }
      if (str_equals(key, key_quiet_mode))    { cs_globals->quiet_mode = value_as_int(value);      return; }
      if (str_equals(key, key_report_introns)){ cs_globals->report_introns = value_as_int(value);  return; }
      if (str_equals(key, key_flush))         { cs_globals->flush = value_as_int(value);           return; }
      if (str_equals(key, key_max_edit_dist)) { cs_globals->max_edit_distance = atoi(value);       return; }
      if (str_equals(key, key_show_bed))      { cs_globals->show_bed = value_as_int(value);        return; }
      if (str_equals(key, key_show_cigar))    { cs_globals->show_cigar = value_as_int(value);      return; }
      if (str_equals(key, key_show_rcigar))   { cs_globals->show_rcigar = value_as_int(value);     return; }
      if (str_equals(key, key_show_sampaf))   { cs_globals->show_sampaf = value_as_int(value);     return; }
      if (str_equals(key, key_show_sortid))   { cs_globals->show_sortid = value_as_int(value);     return; }
      if (str_equals(key, key_show_gcplot))   { cs_globals->show_gcplot = atoi(value);             return; }
      if (str_equals(key, key_ngap_threshold)){ cs_globals->ngap_threshold = atoi(value);          return; }
      if (str_equals(key, key_show_onehot))   { cs_globals->show_onehot = value_as_int(value);     return; }
   } else {
      fprintf(outfile, "%s\n", "All settings must start with a lower case alpha character");
      return;
   }
   fprintf(outfile, "k %s: v %s not recognized and is ignored\n", key, value);
}
float get_sortf(int len, char *str) {
   unsigned long hash = 5381;
   int c;
   while ((c = *str++)) hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
   unsigned long modulo_hash = hash % _SZ;
   float sortf = (float) len + (100.00 / (float) modulo_hash);
   return sortf;
}
Alignment *new_alignment_alloc( ) {
   Alignment *this_aln = (Alignment *) malloc_mem ( sizeof(Alignment) );
   assert (this_aln);
   this_aln->sortf = 0.0;
   this_aln->ed = 0;
   this_aln->hitid[0] = 0;
   this_aln->pattern = NULL;
  
   return this_aln;
}
Alignment **add_alignment (char *_sortid, char *_leader_space, char *_pattern,  unsigned long _ed, char * _hitid, uint64_t _lo, uint64_t _hi, char *_hitseq) { 
   cs_globals->last_aln_idx++;
   Alignment **na = realloc (_alignments, sizeof(Alignment) * cs_globals->last_aln_idx);
   assert (na);
   if (na != _alignments) _alignments = na;
   Alignment *a  = new_alignment_alloc();
   a->ed = _ed;
   a->pattern = new_zero_str(strlen (_pattern) + strlen(_leader_space));
   a->sortf = get_sortf(strlen(_pattern), _sortid); 
   strncpy(a->hitid, _hitid, strlen(_hitid));
   strncpy(a->pattern, _leader_space, strlen(_leader_space));
   strncat(a->pattern, _pattern, strlen(_pattern));
   a->fivep = _hi;
   a->threep = _lo;
   a->hitseq = new_zero_str(strlen(_hitseq));
   strncpy(a->hitseq, _hitseq,strlen(_hitseq));


   _alignments[cs_globals->last_aln_idx - 1] = a;
   return na;
}
void sort() {
    int i, j;
    Alignment *this_aln = NULL;
    for (i = 1; i < cs_globals->last_aln_idx; i++) {
        this_aln = _alignments[i];
        j = i - 1;
        while (j >= 0 && _alignments[j]->sortf > this_aln->sortf) {
            _alignments[j + 1] = _alignments[j];
            j = j - 1;
        }
        _alignments[j + 1] = this_aln;
    }
}
#define POOL_IDX_EXTRACT 0
#define POOL_IDX_DEFLINE 1
void extract_roi (const char *seqname, const char *jobname) {
   uint64_t extract_len;
   char *extract_seq = NULL;
   char *newdefline = NULL;
   uint64_t _locoord;
   uint64_t _hicoord;
   uint64_t _block_size;
   gzFile this_gz;
   kseq_t *this_seq;

   printf("%s,", "RefName");
   this_gz = gzopen(cs_globals->roi_ref_path, "r");
   assert(this_gz);
   this_seq = kseq_init(this_gz);
   extract_len = cs_globals->hicoord - cs_globals->locoord;
   _locoord = cs_globals->locoord;
   _hicoord = cs_globals->hicoord;
   sprintf(cs_globals->roi_file_path, "./%s_%s_%llu_%llu.fa", cs_globals->jobname,seqname, _locoord, _hicoord);
   char *pipepos = strchr(cs_globals->roi_file_path,'|');
   if (pipepos) *pipepos = '_';
   _block_size = cs_globals->roi_block_size;
   gzrewind(this_gz);
   kseq_rewind(this_seq);
   FILE *ef;
   ef = cs_fopen (cs_globals->roi_file_path, _REPLACE);
   assert (ef);
   while (kseq_read(this_seq) >= 0) {
      if (str_equals(seqname, this_seq->name.s)) {
         uint64_t rgn_start;
         uint64_t rgn_end;
         for (rgn_start = _locoord;rgn_start < _hicoord; rgn_start += _block_size) {
            if (rgn_start >= this_seq->seq.l) {
               continue;
            }
            rgn_end = rgn_start + _block_size;
            if (rgn_end  > this_seq->seq.l) rgn_end = this_seq->seq.l - 1;
            
            extract_len = rgn_end - rgn_start;
            extract_seq = string_pool (extract_len + 1, POOL_STR_INIT, POOL_IDX_EXTRACT);
            char *src_str_offset = this_seq->seq.s + rgn_start;
            safe_strcpy(extract_seq, src_str_offset, extract_len);

            newdefline = string_pool (strlen(this_seq->name.s) + 1024, POOL_STR_INIT, POOL_IDX_DEFLINE);
            
            sprintf(newdefline, "%s_%llu-%llu", seqname, rgn_start, rgn_end);
            fprintf(ef, ">%s\n",newdefline);
            fprintf(ef, "%s\n",extract_seq);
            sprintf(newdefline, "%s_%llu-%llu", seqname, rgn_start, rgn_end);
            fprintf(stdout, "%s,", newdefline);
         }
      }
   }
   putchar ('\n');
   fclose (ef);
}
int prep_repel (const char *repel_path) {
	mm_idxopt_t iopt;
	mm_mapopt_t mopt;
   if (file_exists(repel_path)) {
      set_mm_opt_by_analysis_mode(analysis_mode_splice, &cs_globals->repel_idx_opt, &cs_globals->repel_map_opt);
	   mm_set_opt(0, &iopt, &mopt);
	   mopt.flag |= MM_F_CIGAR; // print alignment
      mopt.flag |= MM_F_NO_PRINT_2ND; // no secondary stuff
	   cs_globals->repel_mm_reader = mm_idx_reader_open(cs_globals->repel_path, &iopt, 0);
      fprintf(stderr, "Repel %s opened\n", cs_globals->repel_path);
      if (cs_globals->repel_mm_reader) 
         return 1;
      else
         return 0;
   } else {
      return -1;
   }
}
int has_repel (char *qseq, size_t len, int show_repel) {
	mm_idx_t *mi;
	mm_reg1_t *the_alignments;
   int n_reg, j;
   int repel_cnt = 0;
   if (!cs_globals->repel_mm_reader) return 0;
   while ((mi = mm_idx_reader_read(cs_globals->repel_mm_reader, cs_globals->n_threads)) != 0) { // traverse each part of the index
		mm_tbuf_t *tbuf = mm_tbuf_init(); // thread buffer; for multi-threading, allocate one tbuf for each thread
      the_alignments = mm_map(mi, len, qseq, &n_reg, tbuf, &cs_globals->repel_map_opt, 0); // get all hits for the query
		if (n_reg != 0) { 
         for (j = 0; j < n_reg; ++j) { // traverse hits and print them out
           mm_reg1_t *this_alignment = &the_alignments[j];
           int edit_distance = (this_alignment->blen - this_alignment->mlen) + this_alignment->p->n_ambi;
           float proportion = (float) len / (float) this_alignment->blen;

           if (proportion > 0.05f) {
             repel_cnt++;
             if (show_repel) {
               char *paf = 0;
               fprintf(stderr, "REPEL: ");
               fprintf(stderr, "refnme: %s, qnm:%s  qln:%zu  qst:%d  qen:%d  :srd:%c  ", 
                 cs_globals->current_ref_name, 
                 qseq, 
                 len, 
                 this_alignment->qs, 
                 this_alignment->qe, 
                 "+-"[this_alignment->rev]
               );
               fprintf(stderr,"nme:%s refchrlen:%d  refchrst:%d  refchrend:%d  hln:%d  bln:%d  mpq:%d  nm:i:%d cg:Z:\n", 
                 mi->seq[this_alignment->rid].name,
                 mi->seq[this_alignment->rid].len, 
                 this_alignment->rs, 
                 this_alignment->re, 
                 this_alignment->mlen, 
                 this_alignment->blen, 
                 this_alignment->mapq, 
                 edit_distance 
               );
               int max_buf_len = 0; 
               mm_gen_cs(0, &paf, &max_buf_len, mi, this_alignment, qseq, 0);  // this generates the paf string
               fprintf(stderr, "%s\n", paf);
               free_mem(paf);
             }
           }
         }
      }
      mm_tbuf_destroy(tbuf);
      mm_idx_destroy(mi);
   }
   return repel_cnt;
}
void free_repel (void) {
   mm_idx_reader_close(cs_globals->repel_mm_reader); // close the index reader
}
int prep_cds (const char *cds_path) {
	mm_idxopt_t iopt;
	mm_mapopt_t mopt;
   if (file_exists(cds_path)) {
      set_mm_opt_by_analysis_mode(analysis_mode_splice, &cs_globals->cds_idx_opt, &cs_globals->cds_map_opt);
	   mm_set_opt(0, &iopt, &mopt);
	   mopt.flag |= MM_F_CIGAR; // print alignment
      mopt.flag |= MM_F_NO_PRINT_2ND; // no secondary stuff
	   cs_globals->cds_mm_reader = mm_idx_reader_open(cs_globals->cds_path, &iopt, 0);
      fprintf(stderr, "CDS %s opened\n", cs_globals->cds_path);
      if (cs_globals->cds_mm_reader) 
         return 1;
      else
         return 0;
   } else {
      return -1;
   }
}
int has_cds (char *qseq, size_t len, int show_cds) {
	mm_idx_t *mi;
	mm_reg1_t *the_alignments;
   int n_reg, j;
   int cds_cnt = 0;
   if (!cs_globals->cds_mm_reader) return 0;
   while ((mi = mm_idx_reader_read(cs_globals->cds_mm_reader, cs_globals->n_threads)) != 0) { // traverse each part of the index
		mm_tbuf_t *tbuf = mm_tbuf_init(); // thread buffer; for multi-threading, allocate one tbuf for each thread
      the_alignments = mm_map(mi, len, qseq, &n_reg, tbuf, &cs_globals->cds_map_opt, 0); // get all hits for the query
		if (n_reg != 0) { 
         for (j = 0; j < n_reg; ++j) { // traverse hits and print them out
           mm_reg1_t *this_alignment = &the_alignments[j];
           int edit_distance = (this_alignment->blen - this_alignment->mlen) + this_alignment->p->n_ambi;
           float proportion = (float) len / (float) this_alignment->blen;

           if (proportion > 0.05f) {
             cds_cnt++;
             if (show_cds) {
               char *paf = 0;
               fprintf(stderr, "CDS: ");
               fprintf(stderr, "refnme: %s, qnm:%s  qln:%zu  qst:%d  qen:%d  :srd:%c  ", 
                 cs_globals->current_ref_name, 
                 qseq, 
                 len, 
                 this_alignment->qs, 
                 this_alignment->qe, 
                 "+-"[this_alignment->rev]
               );
               fprintf(stderr,"nme:%s refchrlen:%d  refchrst:%d  refchrend:%d  hln:%d  bln:%d  mpq:%d  nm:i:%d cg:Z:\n", 
                 mi->seq[this_alignment->rid].name,
                 mi->seq[this_alignment->rid].len, 
                 this_alignment->rs, 
                 this_alignment->re, 
                 this_alignment->mlen, 
                 this_alignment->blen, 
                 this_alignment->mapq, 
                 edit_distance 
               );
               int max_buf_len = 0; 
               mm_gen_cs(0, &paf, &max_buf_len, mi, this_alignment, qseq, 0);  // this generates the paf string
               fprintf(stderr, "%s\n", paf);
               free_mem(paf);
             }
           }
         }
      }
      mm_tbuf_destroy(tbuf);
      mm_idx_destroy(mi);
   }
   return cds_cnt;
}
void free_cds (void) {
   mm_idx_reader_close(cs_globals->cds_mm_reader); // close the index reader
}
#define OUT_OF_BOUNDS 0
#define ALL_IN_BOUNDS 1
#define RIGHT_IN_BOUNDS 2
#define LEFT_IN_BOUNDS 3
int is_within_boundaries (uint64_t b1, uint64_t b2, uint64_t w1, uint64_t w2) {
   int lb = 0;
   int rb = 0;
   if (w1 > b1 && w1 < b2) {
      lb = 1;
   }
   if (w2 > b1 && w2 < b2) {
      rb = 1;
   }
   if (rb == 0 && lb == 0) return 0;
   if (lb && rb) return 1;
   if (!lb && rb) return 2;
   if (lb && !rb) return 3;
   return 0;
}
void alpha_counter (int n, char *alphanum) {
   int mod_val = 0;
   int div_val = 0;
   

}
void putpafstr (const char *pafstr, FILE *outstream) {
  int len = strlen(pafstr);
  int i;
  for (i=0;i<len;i++) {
     if (pafstr[i] == '=' ||
         pafstr[i] == '*' ||
         pafstr[i] == '+' ||
         pafstr[i] == '-' ||
         pafstr[i] == '~' ||
         pafstr[i] == ':') {
         fprintf(outstream, "%c", ' ');
     }
   fprintf(outstream, "%c", pafstr[i]);
  }
}
int entry_ison_chrom (const char *entryname, const char *chromename) {
   int len = strlen(chromename);
   int matches = 0;
   int i;
   for (i=0;i<len;i++) {
      if (entryname[i] == chromename[i]) matches++;
   }
   if (matches == len) return 1; else return 0;
}
int conservation_score (const char *seqname, int row) {
	mm_idxopt_t iopt;
	mm_mapopt_t mopt;
   //char qChrName[32];
   float edave = 0.0f;
   int edsum = 0;
   int contigs = 0;
   float fragments = 0.0f;
   char last_contig_hit_name[64];
   uint64_t last_rs;
   uint64_t last_re;
   int col = 0;
   gzFile roif = NULL;
	kseq_t *qryfasta = NULL;
   // mm_verbose = 2; // disable message output to stderr
   set_mm_opt_by_analysis_mode(analysis_mode_asm20, &iopt, &mopt);
	mm_set_opt(0, &iopt, &mopt);
	mopt.flag |= MM_F_CIGAR; // print alignment
   mopt.flag |= MM_F_NO_PRINT_2ND; // no secondary stuff
   mopt.flag |= MM_F_RMQ; // use mm's gap extension EXPERIMENTAL 
   // open query file for reading; you may use your favorite FASTA/Q parser
   if ((strlen(cs_globals->roi_file_path) > 0) && file_exists (cs_globals->roi_file_path)) {
	   roif = gzopen(cs_globals->roi_file_path, "r");
	   assert(roif);
   }  else {
      informative_abort("%s [%s]\n", "Current ROI path not valid", cs_globals->roi_file_path);
   }  
	qryfasta = kseq_init(roif);
   // this requires this state to be set 'query_filer=$roi_file_path'

	// open index reader
   if ((strlen(cs_globals->current_ref_path) > 0) && file_exists (cs_globals->current_ref_path)) {
   }  else {
      informative_abort("%s [%s]\n", "Current ref path not valid", cs_globals->roi_file_path);
   }  
	mm_idx_reader_t *r;
	r = mm_idx_reader_open(cs_globals->current_ref_path, &iopt, 0);
	mm_idx_t *mi;
   fprintf(stderr, "MM2 Indexing for %s\n\n", cs_globals->current_ref_path);
   printf("%s,", cs_globals->current_ref_path);

   while ((mi = mm_idx_reader_read(r, cs_globals->n_threads)) != 0) { // traverse each part of the index
		mm_mapopt_update(&mopt, mi); // this sets the maximum minimizer occurrence; TODO: set a better default in mm_mapopt_init()!
		mm_tbuf_t *tbuf = mm_tbuf_init(); // thread buffer; for multi-threading, allocate one tbuf for each thread
		gzrewind(roif);
		kseq_rewind(qryfasta);
      col = 0;
		while (kseq_read(qryfasta) >= 0) { // each kseq_read() call reads one query sequence
         if (DO_PRINTF_STDERR) fprintf(stderr, ">>> q[%s] r[%s] r[%d:%d] c[%d]\n", qryfasta->name.s, cs_globals->current_ref_path, row, num_refs, col);
			mm_reg1_t *the_alignments;
         int hits = 0;
         int other_chromosomes = 0;
			int j, n_reg;
         int edit_distance = 0;
         int secondary_hit = 0;
         int bounds_check;
         int repels;
         // edsum = 0;
         contigs = 0;
         fragments = 0.0f;
         last_contig_hit_name[0] = 0;
         last_rs = 0;
         last_re = 0;
         the_alignments = mm_map(mi, qryfasta->seq.l, qryfasta->seq.s, &n_reg, tbuf, &mopt, 0); // get all hits for the query
        
			if (n_reg != 0) { 
            contigs = 0;
            hits = 0;
            secondary_hit = 0;
            repels = 0;
            for (j = 0; j < n_reg; ++j) { // traverse hits and print them out
             // get the alignment
              mm_reg1_t *this_alignment = &the_alignments[j];
              assert(this_alignment->p); // with MM_F_CIGAR, this should not be NULL
             
             // calc edit distance
              edit_distance = (this_alignment->blen - this_alignment->mlen) + this_alignment->p->n_ambi;

              // the hit must have less than 5% mismatches
              //if ((float) edit_distance / (float) this_alignment->blen > 0.05f)
              // continue;   // go to the next loop
// SET these as params in cs_globals
              // the hit must be more than 10% of the query len
              float mfrag = 0.0f;

              if (this_alignment->blen > (qryfasta->seq.l * 0.1f))     // hit must be at least more than 10% of the query len
                  mfrag = (float) (this_alignment->mlen - edit_distance) / (float) qryfasta->seq.l;
              else 
                  continue;
             //strip the chromosome number/name from the roi defline name
             // speed thisup using memcmp
             // chr_from_roi_name(qryfasta->name.s, qChrName);
             
             // check for repels
              if ((repels = has_repel(qryfasta->seq.s, qryfasta->seq.l, 1))) {
                  contigs += 1000;
                  if(SHOW_ALIGNMENT) fprintf(stderr, "%s\n\t", "Repeat observed");
              } else {
                  if(SHOW_ALIGNMENT) fprintf(stderr, "%s\n\t", "No Repeats observed");
              }
             // calc the conservation score for this hit
              
              if (!str_equals(cs_globals->seqid, mi->seq[this_alignment->rid].name)) {
                  // edit distance must ben < 10% to be considered a duplication . THIS SHOULD BECOME A VARIABLE
                  other_chromosomes++;
                  if (edit_distance < (this_alignment->mlen * 0.1f)) 
                     contigs++;
              } else {
                  bounds_check = is_within_boundaries (this_alignment->rs, this_alignment->re, last_rs, last_re);
                  if (bounds_check == OUT_OF_BOUNDS) {
                     // add the fragement as it is within the roi but not overlapping with previous hit
                     fragments += mfrag;
                     hits++;
                     edsum += edit_distance;
                  }
                  if (bounds_check != OUT_OF_BOUNDS) {
                    // consiider this a secondary hit for now  as it overlaps with the previous hit
                     secondary_hit++;
                  }
              }
              last_rs = this_alignment->rs;
              last_re = this_alignment->re;
              if (SHOW_ALIGNMENT) {
                char *paf = 0;
                // alignment_score = this_alignment->p->dp_score;
                fprintf(stderr, "\tHITINFO: included = %d", hits);
                fprintf(stderr, "\trefnme=%s:currefchr=%s:qrynme=%s:qryln=%zu:qstart=%d:qend=%d:strnd=%c:", 
                  cs_globals->current_ref_name, 
                  mi->seq[this_alignment->rid].name,
                  qryfasta->name.s, 
                  qryfasta->seq.l, 
                  this_alignment->qs, 
                  this_alignment->qe, 
                  "+-"[this_alignment->rev]
                );
                fprintf(stderr,"\trefchr=%s:refchrlen=%d:refchrst=%d:refchrend=%d:hitlen=%d:boundslen=%d:boundschk=%d:2nary=%d:edtdst=%d:frag=%f:contig_tally=%d:cgar=\n", 
                  mi->seq[this_alignment->rid].name, 
                  mi->seq[this_alignment->rid].len, 
                  this_alignment->rs, 
                  this_alignment->re, 
                  this_alignment->mlen, 
                  this_alignment->blen, 
                  bounds_check,
                  secondary_hit, 
                  edit_distance, 
                  mfrag,
                  contigs
                );
                int max_buf_len = 0; 

 #define SHOW_PAF_IDENTITY 0
 #define SHOW_RUNLENGTH_ENCODING 1
                mm_gen_cs(0, &paf, &max_buf_len, mi, this_alignment, qryfasta->seq.s, SHOW_PAF_IDENTITY);  // this generates the paf string
                fprintf(stderr, "\t\t", stderr);
                putpafstr(paf, stderr);
                fprintf(stderr, "\n", stderr);
                free_mem(paf);
              }
              free(this_alignment->p);
           }
           free(the_alignments);
           
        }
        if (contigs > 0)   // otherwise the fragments is <= 0.0
            fragments += (float) contigs;
        
            
        printf("%f,", fragments);
        col++;
        edave = (float) edsum / contigs;
        if (SHOW_ALIGNMENT)             
           fprintf(stderr, "\tCONSERVATION_CODE: region=%s:copies=%d:contigs=%d:alo_chr=%d:edave=%f:csvscr=%f\n\n", 
            qryfasta->name.s, 
            n_reg, 
            contigs, 
            other_chromosomes,
            edave, 
            fragments
         );
	   }
      putchar('\n');
      fflush (stdout);   // done on the block so as to make sure a block is not broken up
      fflush (stderr);
      mm_tbuf_destroy(tbuf);
      mm_idx_destroy(mi);
   }
   mm_idx_reader_close(r); // close the index reader
   kseq_destroy(qryfasta); // close the query file
   gzclose(roif);
	return 1;
}
#define RUNNAME_ARG 1
#define ROI_REF_ARG 2
#define ROI_CHR_ARG 3
#define ROI_LO_ARG 4
#define ROI_HI_ARG 5
#define ROI_CHUNK_ARG 6
#define REPEL_PATH_ARG 7
#define DATASET_1_ARG 8
#define TEST_FASTA_CREATION 0
int main (int argc, char *argv[]) {
   if (argc == 1)  {
      fprintf(stdout, "Usage: fanfare <Job name> <ROI Genome> <ROI Chromosome> <ROI lo> <ROI hi> <Chunk size> <repel path> <dataset1> ... <datasetN>\n");
      exit (0);
   }
   cs_globals = (Globals *) malloc (sizeof(Globals));
   assert (cs_globals);
   setup();
   safe_strcpy(cs_globals->jobname, argv[RUNNAME_ARG], strlen(argv[RUNNAME_ARG])); 
   safe_strcpy(cs_globals->roi_ref_path, argv[ROI_REF_ARG], strlen(argv[ROI_REF_ARG])); 
   safe_strcpy(cs_globals->repel_path, argv[REPEL_PATH_ARG], strlen(argv[REPEL_PATH_ARG])); 
   safe_strcpy(cs_globals->seqid, argv[ROI_CHR_ARG], strlen(argv[ROI_CHR_ARG])); 
   cs_globals->locoord = (uint64_t) atoi(argv[ROI_LO_ARG]);
   if (cs_globals->locoord == 0) {
      fprintf(stderr, "Did you forget the locoord? [%llu]\n", cs_globals->locoord);
      exit (0);
   }
   cs_globals->hicoord = (uint64_t) atoi(argv[ROI_HI_ARG]);
   if (cs_globals->hicoord == 0) {
      fprintf(stderr, "Did you forget the hicoord? [%llu]\n", cs_globals->hicoord);
      exit (0);
   }
   cs_globals->roi_block_size = (uint64_t) atoi(argv[ROI_CHUNK_ARG]);
   if (cs_globals->roi_block_size == 0) {
      fprintf(stderr, "Did you forget the block size? [%llu]\n", cs_globals->roi_block_size);
      exit (0);
   }
   
   if (cs_globals->locoord + cs_globals->roi_block_size > cs_globals->hicoord) {
      fprintf(stderr, "You're block size is > the ROI. Using the ROI size\n");
      cs_globals->roi_block_size = cs_globals->hicoord = cs_globals->locoord;
   }
   fprintf(stderr, "extracting sequences[%s  |  %s]\n",cs_globals->roi_ref_path, cs_globals->jobname);
   extract_roi(cs_globals->seqid, cs_globals->jobname);
   if (TEST_FASTA_CREATION) {
      exit (0);
   }
   fprintf(stderr, "Indexing repel at path: %s\n",cs_globals->repel_path);
   prep_repel(cs_globals->repel_path);
   int argi = DATASET_1_ARG;
   num_refs = argc - argi;
   fprintf(stderr, "Getting into analysis %d %d\n", argi, argc);
   char bname[1024];
   for (;argi < argc;argi++) {
      fprintf(stderr, "Analysing %s\n",cs_globals->current_ref_path );
      cs_globals->current_ref_path[0] = 0;
      safe_strcpy(cs_globals->current_ref_path, argv[argi], strlen(argv[argi]));
      
      basename_r(cs_globals->current_ref_path, bname);
      strncpy(cs_globals->current_ref_name, bname, strlen(bname));
      conservation_score(cs_globals->seqid, argi - DATASET_1_ARG);
   }
   breakdown();

   free_repel();
}
