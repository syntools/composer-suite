#!/bin/sh

processor=`uname -p`

datestamp=`date "+%Y-%m-%d-%H-%M-%S"`
binpath=/Volumes/data/project/syntools/minimap2_copland/.bu

if [ $processor == "arm" ] 
then
   make arm_neon=1 aarch64=1 $1 
fi
if [ $processor == "i386" ] 
then
   make $1
fi
newbin=$1+='_'
newbin+=$datestamp
cp $1 $binpath/$1_$datestamp
# ./barber test
