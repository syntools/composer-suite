#ifndef MAL_COMPOSERS_H
#define MAL_COMPOSERS_H

#ifdef __cplusplus
extern "C" {
#endif

#define CS_HIT_PERFECT      0x001 
#define CS_HIT_FRAGMENT     0x002 
#define CS_HIT_OTHER_CHR    0x004
#define CS_HIT_OTHER_LOC    0x008
#define CS_HIT_REPEL        0x010
#define CS_HIT_CODING       0x020
#define CS_HIT_TELOMERE     0x040
#define CS_HIT_WATSON       0x080 
#define CS_HIT_CRICK        0x100 
#define CS_HIT_REF_INSERT   0x200 
#define CS_HIT_QRY_INSERT   0x400
#define CS_HIT_ALO_CHR      0x800
#define MM_F_SR            0x1000
#define MM_F_FRAG_MODE     0x2000
#define MM_F_NO_PRINT_2ND  0x4000
#define MM_F_2_IO_THREADS  0x8000
#define MM_F_LONG_CIGAR    0x10000
#define MM_F_INDEPEND_SEG  0x20000
#define MM_F_SPLICE_FLANK  0x40000
#define MM_F_SOFTCLIP      0x80000
#define MM_F_FOR_ONLY      0x100000
#define MM_F_REV_ONLY      0x200000
#define MM_F_HEAP_SORT     0x400000
#define MM_F_ALL_CHAINS    0x800000
#define MM_F_OUT_MD        0x1000000
#define MM_F_COPY_COMMENT  0x2000000
#define MM_F_EQX           0x4000000 // use =/X instead of M
#define MM_F_PAF_NO_HIT    0x8000000 // output unmapped reads to PAF
#define MM_F_NO_END_FLT    0x10000000
#define MM_F_HARD_MLEVEL   0x20000000
#define MM_F_SAM_HIT_ONLY  0x40000000

#ifdef __cplusplus
}
#endif

#endif
