//head and put the Rudy card on file if you want to pay for it To compile:
//   gcc -g -O2 example.c libminimap2.a -lz

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <math.h>
#include <zlib.h>
#include <pthread.h>
#include "minimap.h"
#include "kseq.h"
#include "mmpriv.h"
#include "composers.h"
#include <uuid/uuid.h>

#define _PF_DEBUG 1
#define MAX_HIT_LEN_DIVISOR 10000.00
#define ASCII_CODE_0 48
#define THIS_PROGRAM_NAME "barber"
#define TESTMODE 1
// __builtin_frame_address(0)

KSEQ_INIT(gzFile, gzread)

FILE *outfile;
Globals *cs_globals;
pthread_mutex_t lock;

kseq_t *roi_seq;
gzFile roi_gz;
kseq_t *repel_seq;
gzFile repel_gz;
kseq_t *cds_seq;
gzFile cds_gz;
kseq_t *current_ref_seq;
gzFile current_ref_gz;
static inline int str_empty (const char *str) { return (str[0] == 0); }
static inline int str_equals (const char *str1, const char *str2) { return (strcmp(str1, str2) == 0); }
static inline char *new_zero_str(unsigned long size) { char *ns = (char *) malloc(size + 1); assert(ns); ns[0] = 0;  return ns; } 
static inline void free_mem (void *ptr) { if (ptr) { free (ptr);  }}
void *malloc_mem(uint64_t size) { void *p = malloc(size); if (p)  return p; else return NULL; }
void chop_off_newline(char *s) { register size_t i; register size_t l = strlen(s); for(i=0;i<l;i++) if (s[i] == '\n') { s[i] = 0; return; } }
#define _SZ 32769
char *string_pool (unsigned long size, int init, int caller_id) {
  char* this_str = cs_globals->str_pool[caller_id];
  unsigned long this_sz = cs_globals->str_pool_sz[caller_id];
  pthread_mutex_lock(&lock);
  if (this_sz == 0 && size > 0) {
     if (cs_globals->str_pool[caller_id] != NULL)
        free (cs_globals->str_pool[caller_id]);

     cs_globals->str_pool[caller_id] = new_zero_str(size);
     cs_globals->str_pool_sz[caller_id] = size;
     return cs_globals->str_pool[caller_id];
  }
  if (this_str && (this_sz >= size)) {
     if (init)
        this_str[0] = 0;
     return this_str;
  }
  if (this_str && (this_sz < size)) {
     char *newstr =  realloc(cs_globals->str_pool[caller_id], size);
     if (newstr) {
        cs_globals->str_pool[caller_id] = newstr;
        cs_globals->str_pool_sz[caller_id] = size;
        return newstr;
     } else {
        return NULL;
     }
  }
  pthread_mutex_unlock(&lock);
  return NULL;
}


void safe_strcpy(char* dest, const char* src, size_t dest_size) {
    pthread_mutex_lock(&lock);
    // Assume dest_size is the total size of the destination buffer.
    strncpy(dest, src, dest_size); // Leave space for the null terminator
    dest[dest_size] = '\0';        // Ensure null termination
    pthread_mutex_unlock(&lock);
}

// Initialize the mutex before using safe_strcpy in a multithreaded context

void informative_abort(const char *format, const char *info1, const char *info2) {
      fprintf (stderr, format, info1, info2);
      fprintf (outfile, format, info1, info2);
      fflush(outfile);
      exit(0); 
}
int file_exists (const char *path) { 
   if (access(path, F_OK) != 0) {
      informative_abort("File %s does not exist [%s]\n",path, "");
      return 0;
   } else return 1; 
}
#define UUID_CALLER 10
char *uuid_pool (void) {
    uuid_t uuid;
    char *uuid_str  = string_pool (37, 1, UUID_CALLER);
    // Generate a UUID
    uuid_generate(uuid);

    // Convert the binary UUID to a string
    uuid_unparse_lower(uuid, uuid_str);
    return uuid_str;
}
char *timestamp (int _long) {
    char ts[PATH_MAX];
    ts[0] = 0;
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    if (_long) 
      sprintf(ts, "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    else 
      sprintf(ts, "%d-%02d-%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
    return strdup(ts);
}
float get_sortf(int len, char *str) {
   unsigned long hash = 5381;
   int c;
   while ((c = *str++)) hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
   unsigned long modulo_hash = hash % _SZ;
   float sortf = (float) len + (100.00 / (float) modulo_hash);
   return sortf;
}
void onehot_printer (char *topstr, char *botstr) {
   int len = strlen(topstr);
#define ONEHOT_CALLER 3
   int i;
   for (i=0;i<len;i++) {
      if (botstr[i] == '.') {
         if (topstr[i] == 'A') { fprintf(outfile, "[1. 0. 0. 0. ]"); continue; }
         if (topstr[i] == 'C') { fprintf(outfile, "[0. 1. 0. 0. ]"); continue; }
         if (topstr[i] == 'G') { fprintf(outfile, "[0. 0. 1. 0. ]"); continue; }
         if (topstr[i] == 'T') { fprintf(outfile,"[0. 0. 0. 1. ]"); continue; }
         if (topstr[i] == 'N') { fprintf(outfile,"[0. 0. 0. 0. ]"); continue; }
      } else {
         if (botstr[i] == 'a') { fprintf(outfile,"[1. 0. 0. 0. ]"); continue; }
         if (botstr[i] == 'c') { fprintf(outfile,"[0. 1. 0. 0. ]"); continue; }
         if (botstr[i] == 'g') { fprintf(outfile,"[0. 0. 1. 0. ]"); continue; }
         if (botstr[i] == 't') { fprintf(outfile,"[0. 0. 0. 1. ]"); continue; }
         if (botstr[i] == 'n') { fprintf(outfile,"[0. 0. 0. 0. ]"); continue; }
      }
   }
}
int set_mm_opt_by_analysis_mode(char  *mode, mm_idxopt_t *io, mm_mapopt_t *mo) {
    if (str_equals(mode,analysis_mode_none)) {
        // if (io) mm_idxopt_init_cs (io);
        // if (mo) mm_mapopt_init_cs (mo);
    } else if (str_equals(mode, analysis_mode_ava_ont)) {
        if (io) io->flag = 0;
        if (io) io->k = 15;
        if (io) io->w = 5;
        if (mo) mo->flag |= MM_F_ALL_CHAINS | MM_F_NO_DIAG | MM_F_NO_DUAL | MM_F_NO_LJOIN | MM_F_CIGAR | MM_F_OUT_MD;
        if (mo) mo->min_chain_score = 100; if (mo) mo->pri_ratio = 0.0f; if (mo) mo->max_gap = 10000; if (mo) mo->max_chain_skip = 25;
        if (mo) mo->bw = 2000;
    } else if (str_equals(mode,  analysis_mode_asm5) || str_equals(mode, analysis_mode_hapblock)) {
        if (io) io->flag = 0;
        if (io) io->k = 19;
        if (io) io->w = 19;
        if (mo) mo->a = 1;
        if (mo) mo->b = 19;
        if (mo) mo->q = 39;
        if (mo) mo->q2 = 81;
        if (mo) mo->e = 3;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 200;
        if (mo) mo->min_mid_occ = 100;
        if (mo) mo->min_dp_max = 200;
        if (mo) mo->best_n = 50;
    } else if (str_equals(mode, analysis_mode_asm10)) {
        if (io) io->flag = 0;
        if (io) io->k = 19;
        if (io) io->w = 19;
        if (mo) mo->a = 1;
        if (mo) mo->b = 9;
        if (mo) mo->q = 16;
        if (mo) mo->q2 = 41;
        if (mo) mo->e = 2;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 200;
        if (mo) mo->min_mid_occ = 100;
        if (mo) mo->min_dp_max = 200;
        if (mo) mo->best_n = 50;
    } else if (str_equals(mode,analysis_mode_asm20)) {
        if (io) io->flag = 0;
        if (io) io->k = 19;
        if (io) io->w = 10;
        if (mo) mo->a = 1;
        if (mo) mo->b = 4;
        if (mo) mo->q = 6;
        if (mo) mo->q2 = 26;
        if (mo) mo->e = 2;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 200;
        if (mo) mo->min_mid_occ = 100;
        if (mo) mo->min_dp_max = 200;
        if (mo) mo->best_n = 50;
    } else if (str_equals(mode,analysis_mode_acr)
               || str_equals( mode , analysis_mode_sr)
               ) {
        if (io) io->flag = 0;
        if (io) io->k = 13;
        if (io) io->w = 8;
//        if (io) io->k = 15;
//        if (io) io->w = 5;
        if (mo) mo->flag |= MM_F_SR
                | MM_F_FRAG_MODE
                | MM_F_2_IO_THREADS
                | MM_F_HEAP_SORT
                | MM_F_OUT_CS
                | MM_F_CIGAR
                | MM_F_OUT_MD;
        if (mo) mo->pe_ori = 0<<1|1; // FR
        if (mo) mo->a = 2;
        if (mo) mo->b = 8;
        if (mo) mo->q = 12;
        if (mo) mo->e = 2;
        if (mo) mo->q2 = 24;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 100;
        if (mo) mo->end_bonus = 10;
        if (mo) mo->max_frag_len = 800;
        if (mo) mo->max_gap = 100;
        if (mo) mo->bw = 100;
        if (mo) mo->pri_ratio = 0.5f;
        if (mo) mo->min_cnt = 2;
        if (mo) mo->min_chain_score = 25;
        if (mo) mo->min_dp_max = 40;
        if (mo) mo->best_n = 20;
        if (mo) mo->mid_occ = 1000;
        if (mo) mo->max_occ = 5000;
        if (io) io->bucket_bits = 14;
        if (io) io->batch_size = 4000000000ULL;
        if (mo) mo->mini_batch_size = 50000000;
        // if (str_equals(mode, analysis_mode_skim_map))
        //     if (mo) mo->sc_ambi = 1;
    } else if (str_equals( mode, analysis_mode_cds) 
               || str_equals(mode, analysis_mode_splice)
               || str_equals(mode, analysis_mode_splice_hq)) {
        if (io) io->flag = 0;
        if (io) io->k = 15;
        if (io) io->w = 5;
        if (mo) mo->flag |= MM_F_SPLICE
                        | MM_F_SPLICE_FOR
                        | MM_F_SPLICE_REV
                        | MM_F_SPLICE_FLANK
                        | MM_F_CIGAR
                        | MM_F_OUT_MD;
        if (mo) mo->max_gap = 2000;
        if (mo) mo->max_gap_ref = mo->bw = 200000;
        if (mo) mo->a = 1;      //match score
        if (mo) mo->b = 2;      // mismatch score
        if (mo) mo->q = 2;
        if (mo) mo->e = 1;
        if (mo) mo->q2 = 32;
        if (mo) mo->e2 = 0;
        if (mo) mo->noncan = 9;
        if (mo) mo->junc_bonus = 9;
        if (mo) mo->zdrop = 200;
        if (mo) mo->zdrop_inv = 100; // because mo->a is halved
        if (str_equals(mode, analysis_mode_splice_hq)) {
            if (mo) mo->junc_bonus = 5;
            if (mo) mo->b = 4;
            if (mo) mo->q = 6;
            if (mo) mo->q2 = 24;
        }
    } else return -1;
    return 0;
}
char *gc_profile_pool (char *_topstr, char *_botstr, uint32_t window_size) {
   int len = strlen(_topstr);
#define GCPROFILE_CALLER 4
   char *gc_plot = string_pool(len, 1, GCPROFILE_CALLER);
   int i,j;
   int A,C,G,T,N;
   int _half = window_size / 2;
   for (i=0;i<len;i++) {
      for (A=0,C=0,G=0,T=0,N=0,j=i-_half;j<i+_half;j++) {
         if (j >=0 && j < len) {
            if (_botstr == NULL || _botstr[j] == '.') {
               if (_topstr[j] == 'A') { A++; continue; }
               if (_topstr[j] == 'C') { C++; continue; }
               if (_topstr[j] == 'G') { G++; continue; }
               if (_topstr[j] == 'T') { T++; continue; }
            } else {
               if (_botstr[j] == 'a') { A++; continue; }
               if (_botstr[j] == 'c') { C++; continue; }
               if (_botstr[j] == 'g') { G++; continue; }
               if (_botstr[j] == 't') { T++; continue; }
            }
         }
      }
      float gc_ave = (float) (G + C) / (A + T + G + C);
      long int gc_int = lrintf(gc_ave * 10.0);
      gc_plot[i] = (char) gc_int + ASCII_CODE_0;
   }
   gc_plot[i+1] = 0;
   return gc_plot;
}
Alignment *new_alignment_alloc( ) {
   Alignment *this_aln = (Alignment *) malloc_mem ( sizeof(Alignment) );
   assert (this_aln);
   this_aln->sortf = 0.0;
   this_aln->ed = 0;
   this_aln->hitid[0] = 0;
   this_aln->pattern = NULL;
  
   return this_aln;
}
Alignment **add_alignment (char *_sortid, char *_leader_space, char *_pattern,  unsigned long _ed, char * _hitid, uint64_t _lo, uint64_t _hi, char *_hitseq) { 
   cs_globals->last_aln_idx++;
   Alignment **na = realloc (_alignments, sizeof(Alignment) * cs_globals->last_aln_idx);
   assert (na);
   if (na != _alignments) _alignments = na;
   Alignment *a  = new_alignment_alloc();
   a->ed = _ed;
   a->pattern = new_zero_str(strlen (_pattern) + strlen(_leader_space));
   a->sortf = get_sortf(strlen(_pattern), _sortid); 
   strncpy(a->hitid, _hitid, strlen(_hitid));
   strncpy(a->pattern, _leader_space, strlen(_leader_space));
   strncat(a->pattern, _pattern, strlen(_pattern));
   a->fivep = _hi;
   a->threep = _lo;
   a->hitseq = new_zero_str(strlen(_hitseq));
   strncpy(a->hitseq, _hitseq,strlen(_hitseq));


   _alignments[cs_globals->last_aln_idx - 1] = a;
   return na;
}
void sort() {
    int i, j;
    Alignment *this_aln = NULL;
    for (i = 1; i < cs_globals->last_aln_idx; i++) {
        this_aln = _alignments[i];
        j = i - 1;
        while (j >= 0 && _alignments[j]->sortf > this_aln->sortf) {
            _alignments[j + 1] = _alignments[j];
            j = j - 1;
        }
        _alignments[j + 1] = this_aln;
    }
}
void print_alignments() { 
   float last_sortf = 0.00;
   int sort_rank = 0;
   size_t i;
   char *timestr;

   timestr = timestamp(1);
   fprintf(stderr, "%s %s\n", "Sorting...", timestr);
   free (timestr);
   sort();
   timestr = timestamp(1);
   fprintf(stderr, "%s %s\n", "...Complete", timestr);
   free (timestr);
   for (i=0;i<cs_globals->last_aln_idx;i++) {
      if (_alignments[i]->hitid[0] != 0 && _alignments[i]->pattern)  {
      	if (_alignments[i]->sortf != last_sortf) {
     	      sort_rank++;
     	   }
     	   fprintf (outfile, "GROUPING %4d)", sort_rank);
     	   fprintf (outfile, "  %s %42s",  _alignments[i]->hitid, _alignments[i]->hitseq);
     	   fprintf (outfile, ": '%s'", _alignments[i]->pattern);
     	   fprintf (outfile, " [%f]\n", _alignments[i]->sortf);
     	   last_sortf = _alignments[i]->sortf;
      }
   }
}
uint32_t count_ns(char* file) {
    if (cs_globals->ngap_threshold < 9) return -1;
    FILE *f = fopen(file, "r");
    char buf[_SZ];
    register uint32_t cnt = 0;
    int run_sz = 0;
    for(;;) {
        register size_t res = fread(buf, 1, _SZ, f);
        if (ferror(f)) return -1;
        register int i;
        for(i = 1; i < res; i++) {
            if (buf[i] == 'N') {
               if (run_sz > cs_globals->ngap_threshold) continue;
               if ( buf[i - 1] == 'N') {
                  run_sz++;
               }
            } else {
               run_sz = 0;
            }
            if (run_sz == cs_globals->ngap_threshold) 
               cnt++;
        }
        if (feof(f)) break;
    }
    fclose (f);
    return cnt;
}

void make_snake_name(char *str) {
   register int len = strlen(str);
   register int i;
   for (i=0;i<len;i++) {
      switch (str[i]) {
         case '|':
         case '$':
         case '!':
         case '@':
         case '(':
         case ')':
         case '*':
         case '&':
         case ',':
         case '/':
         case '\\':
            str[i] = '_';
            break;
      }
   }
}
FILE *cs_fopen(const char * restrict path, int _MODE) {
   if (_MODE == _APPEND) return fopen(path,"a");
   if (_MODE == _REPLACE) return fopen(path,"w");
   if (_MODE == _READ_ONLY) return fopen(path,"r");
   return NULL;
   // this is a wrapper for future file locking code
}
void unload_seq_file (char *value) {
   if (str_equals(value, CMD_VALUE_CURRENT_REF)) {
	   if (current_ref_seq) kseq_destroy(current_ref_seq); // close the query file
	   if (current_ref_gz) gzclose(current_ref_gz);
      current_ref_seq = NULL;
      current_ref_gz = NULL;
   }
   if (str_equals(value, CMD_VALUE_ROI)) {
	   if (roi_seq) kseq_destroy(roi_seq); // close the query file
	   if (roi_gz) gzclose(roi_gz);
      roi_seq = NULL;
      roi_gz = NULL;
   }
   if (str_equals(value, CMD_VALUE_REPEL)) {
	   if (repel_seq) kseq_destroy(repel_seq); // close the query file
	   if (repel_gz) gzclose(repel_gz);
      repel_seq = NULL;
      repel_gz = NULL;
   }
   if (str_equals(value, CMD_VALUE_CDS)) {
	   if (cds_seq) kseq_destroy(cds_seq); // close the query file
	   if (cds_gz) gzclose(cds_gz);
      cds_seq = NULL;
      cds_gz = NULL;
   }
}
void load_seq_file (char *value) {
   if (str_equals(value, CMD_VALUE_CURRENT_REF)) {
      if (str_empty(cs_globals->current_ref_path)) {
         informative_abort("%s [%s]\n", "current_ref_path not set", "");
      }
      current_ref_gz = gzopen(cs_globals->current_ref_path, "r");
      assert(current_ref_gz);
      current_ref_seq = kseq_init(current_ref_gz);
   }
   if (str_equals(value, CMD_VALUE_ROI)) {
      if (str_empty(cs_globals->roi_ref_path) || str_empty(cs_globals->seqid)) {
         informative_abort("Roi ref path [%s] or SeqID [%s] not set\n", cs_globals->roi_ref_path, cs_globals->seqid);
      }
      if (roi_seq) {
         if (!strstr(roi_seq->name.s, cs_globals->roi_ref_path)) {
            unload_seq_file(value);        
         }
      }
      roi_gz = gzopen(cs_globals->roi_ref_path, "r");
      assert(roi_gz);
      roi_seq = kseq_init(roi_gz);
   }
   if (str_equals(value, CMD_VALUE_REPEL)) {
      if (str_empty(cs_globals->repel_path)) {
         informative_abort("Repel file [%s] not set [%s]\n", cs_globals->repel_path, "");
      }
      if (repel_seq) {
         if (!strstr(repel_seq->name.s, cs_globals->repel_path)) {
            unload_seq_file(value);        
         }
      }
      repel_gz = gzopen(cs_globals->repel_path, "r");
      assert(repel_gz);
      repel_seq = kseq_init(repel_gz);
   }
   if (str_equals(value, CMD_VALUE_CDS)) {
      if (str_empty(cs_globals->cds_path)) {
         informative_abort("CDS path[%s] not set [%s]\n", cs_globals->cds_path, "");
      }
      if (cds_seq) {
         if (!strstr(cds_seq->name.s, cs_globals->cds_path)) {
            unload_seq_file(value);        
         }
      }
      cds_gz = gzopen(cs_globals->cds_path, "r");
      assert(cds_gz);
      cds_seq = kseq_init(cds_gz);
   }
}
void reverse (char *dna) {
   register int l = strlen (dna);
   register int i;
    for (i=0;i<l;i++) {
        switch (dna[i]) {
            case 'A':
                dna[i] = 'T';
                break;
            case 'a':
                dna[i] = 't';
                break;
            case 'C':
                dna[i] = 'G';
                break;
            case 'c':
                dna[i] = 'g';
                break;
            case 'G':
                dna[i] = 'C';
                break;
            case 'g':
                dna[i] = 'c';
                break;
            case 'T':
                dna[i] = 'A';
                break;
            case 't':
                dna[i] = 'a';
                break;
            case '.':
                dna[i] = '.';
                break;
            case 'N':
                dna[i] = 'N';
                break;
            case 'n':
                dna[i] = 'n';
                break;
            default:
                break;
        }
    }
}
#define REVCOMP_CALLER 9
char *revcomp_pool (char *instr) {
    register long len = strlen(instr);
    char *outstr = string_pool(len, 1, REVCOMP_CALLER);
    register long i = len;
    register long j = 0;
    for (i=len;i>=0 && j < len;j++, i--) {
        switch (instr[i -1]) {
            case 'A':
                outstr[j] = 'T';
                break;
            case 'a':
                outstr[j] = 't';
                break;
            case 'C':
                outstr[j] = 'G';
                break;
            case 'c':
                outstr[j] = 'g';
                break;
            case 'G':
                outstr[j] = 'C';
                break;
            case 'g':
                outstr[j] = 'c';
                break;
            case 'T':
                outstr[j] = 'A';
                break;
            case 't':
                outstr[j] = 'a';
                break;
            case '.':
                outstr[j] = '.';
                break;
            case 'N':
                outstr[j] = 'N';
                break;
            case 'n':
                outstr[j] = 'n';
                break;
            default:
                outstr[j] = instr[i - 1];
                break;
        }
    }
    outstr[j] = 0;
    return outstr;
}
#define CS2MPAS_R_CALLER 7
#define CS2MPAS_A_CALLER 8
char *cs2mpas_entry_pool (const char *cs_str, int top, int revcompliment) {
    int cs_mode = 0;
    int pretog = 0, tog = 1;
    register unsigned long slen = strlen(cs_str);
    register unsigned long i,j;
    char *topstr = string_pool(slen, 1, CS2MPAS_R_CALLER);
    char *botstr = string_pool(slen, 1, CS2MPAS_A_CALLER);
    topstr[0] = 0;
    botstr[0] = 0;
    for (i=0, j=0;i < slen;i++) {
        switch (cs_str[i]) {
            case '=': 
               cs_mode = 1; tog = 0; continue;
               break;
            case ':':  
               cs_mode = 2; tog = 0; continue; 
               break;
            case '*': 
               cs_mode = 3; tog = 0; continue; 
               break;
            case '+': 
               cs_mode = 4; tog = 0; continue; 
               break;
            case '-':  
               cs_mode = 5; tog = 0; continue;
               break;
            case '~': 
               cs_mode = 6; tog = 1; continue;
               break;
        }
        if (cs_mode != 6) {
           if (tog < pretog) {
               topstr[j] = ']';
               botstr[j] = ']';
               
               topstr[++j] = 0;
               botstr[j] = 0;
           }
        }
        if (cs_mode == 6) {
            if (pretog < tog) {
               topstr[j] = '[';
               botstr[j] = '[';
               
               topstr[++j] = 0;
               botstr[j] = 0;
               
            }
        }
        switch (cs_mode) {
            case 1:
                topstr[j] = cs_str[i];
                botstr[j] = '.';
                topstr[++j] = 0;
                botstr[j] = 0;
                break;
            case 2:
                topstr[j] = cs_str[i];
                botstr[j] = '.';
                topstr[++j] = 0;
                botstr[j] = 0;
                break;
            case 3:
                topstr[j] = cs_str[i++];
                botstr[j] = cs_str[i];
                topstr[++j] = 0;
                botstr[j] = 0;
                break;
            case 4:
                topstr[j] = cs_str[i];
                botstr[j] = '-';
                topstr[++j] = 0;
                botstr[j] = 0;
                break;
            case 5:
                topstr[j] = '-';
                botstr[j] = cs_str[i];
                topstr[++j] = 0;
                botstr[j] = 0;
                break;
            case 6:
                topstr[j] = '~';
                botstr[j] = cs_str[i];
                topstr[++j] = 0;
                botstr[j] = 0;
                break;
            default:
                break;
        }
        pretog = tog;
        if (j >= slen) {
            topstr = string_pool(j + 1, 0, CS2MPAS_R_CALLER);
            botstr = string_pool(j + 1, 0, CS2MPAS_A_CALLER);
        }
    }
    botstr[strlen(topstr)] = 0;
    if (top > 0) {
      return topstr;
    } else {
      return botstr;
    }
}
char *bed_line_pool (char *chrom, int start, int end, int hit_num, int score, char strand) {
#define BEDLINE_CALLER 5
    char *beds = string_pool(_LABEL_MAX, 1, BEDLINE_CALLER);
    snprintf (beds, _LABEL_MAX, "%s\t%d\t%d\thit_num_%d\t%d\t%c", chrom, start, end + 1, hit_num, score, strand);
    return beds;
}
char *rcigar_pool (char *refstr, char *qrystr) {
   register int len = strlen(qrystr);
#define RCIGAR_CALLER 6
   char *rcig_str = string_pool(len, 1, RCIGAR_CALLER);
   register int i;
   register int j;
   for (i=0,j=0;i<len;i++) {
     if (refstr[i] == '~') {
        if (i == 0)
           rcig_str[j++] = '~', rcig_str[j] = 0;
        else if (refstr[i - 1] != '~')
           rcig_str[j++] = '~', rcig_str[j] = 0;
        continue;
     } 
     if (qrystr[i] == '.') {
        if (i == 0)
           rcig_str[j++] = '.', rcig_str[j] = 0;
        else if (qrystr[i - 1] != '.')
           rcig_str[j++] = '.', rcig_str[j] = 0;
        continue;
     } 
     if ((qrystr[i] == 'a' && refstr[i] == 'g') || (qrystr[i] == 'g' && refstr[i] == 'a') || (qrystr[i] == 'c' && refstr[i] == 't') || (qrystr[i] == 't' && refstr[i] == 'c')) {
         rcig_str[j++] = 'X', rcig_str[j] = 0;
         continue;
     } else  {
         rcig_str[j++] = 'x', rcig_str[j] = 0;     // transversion
         continue;
     }
     if (refstr[i] == '-' && qrystr[i] != '.') {
         if ((i - 1) > -1 && refstr[i - 1] == '-')  continue;
         rcig_str[j++] = 'D', rcig_str[j] = 0;
         continue;
     }
     if (qrystr[i] == '-' && refstr[i] != '-') {
         if (qrystr[i - 1] == '-')  continue;
         rcig_str[j++] = 'I',  rcig_str[j] = 0;
         continue;
     }
   }
   return rcig_str;
}
#define format_string "# ## %24s: %s\n"
#define format_number "# ## %24s: %d\n"
void diagnostics () {
      fprintf(outfile,  format_string, key_job, cs_globals->jobname);                      fprintf(outfile,  format_number, key_threads,cs_globals->n_threads);                    fprintf(outfile,  format_string, key_repel_file, cs_globals->repel_path);        
      fprintf(outfile,  format_string, key_cds_file, cs_globals->cds_path);                fprintf(outfile,  format_string, key_roi_ref_file, cs_globals->roi_ref_path);           fprintf(outfile,  format_string, key_ref_file, cs_globals->current_ref_path); 
      fprintf(outfile,  format_string, key_username, cs_globals->username);                fprintf(outfile,  format_string, key_mode, cs_globals->analysis_mode);                  fprintf(outfile,  format_string, key_roi_seqid, cs_globals->seqid);                  
      fprintf(outfile,  format_number, key_roi_lo, (int) cs_globals->locoord);             fprintf(outfile,  format_number, key_roi_hi, (int) cs_globals->hicoord);                fprintf(outfile,  format_number, key_fivep_extra, (int) cs_globals->fivep_extra);             
      fprintf(outfile,  format_number, key_threep_extra, (int) cs_globals->threep_extra);  fprintf(outfile,  format_number, key_roi_block_size, (int) cs_globals->roi_block_size); fprintf(outfile,  format_string, key_query_file, cs_globals->query_path);             
      fprintf(outfile,  format_string, key_results_file, cs_globals->results_path);        fprintf(outfile,  format_string, key_mpas_file_path, cs_globals->mpas_file_path);       fprintf(outfile,  format_string, key_roi_file, cs_globals->roi_file_path);          
      fprintf(outfile,  format_number, key_keep_roi_file, (int) cs_globals->keep_roi_file);fprintf(outfile,  format_number, key_extract_mode, (int) cs_globals->extract_mode);     fprintf(outfile,  format_number, key_quiet_mode, cs_globals->quiet_mode);      
      fprintf(outfile,  format_number, key_report_introns, cs_globals->report_introns);    fprintf(outfile,  format_number, key_flush, cs_globals->flush);                         fprintf(outfile,  format_number, key_max_edit_dist, (int) cs_globals->max_edit_distance);       
      fprintf(outfile,  format_number, key_show_bed, cs_globals->show_bed);                fprintf(outfile,  format_number, key_show_cigar, cs_globals->show_cigar);               fprintf(outfile,  format_number, key_show_rcigar, cs_globals->show_rcigar);     
      fprintf(outfile,  format_number, key_show_sampaf, cs_globals->show_sampaf);          fprintf(outfile,  format_number, key_show_sortid, cs_globals->show_sortid);             fprintf(outfile,  format_number, key_show_gcplot, cs_globals->show_gcplot);     
      fprintf(outfile,  format_number, key_ngap_threshold, cs_globals->ngap_threshold);    fprintf(outfile,  format_number, key_show_onehot, cs_globals->show_onehot);     
}
void setup ( ) {
   int i;
   for (i=0;i<_STR_POOL_MAX;i++) {
      cs_globals->str_pool[i] = NULL;
      cs_globals->str_pool_sz[i] = 0;
   }
   char call[_PATH_MAX];
   cs_globals->config_path = new_zero_str (_CALL_STR_MAX);
   cs_globals->jobname = new_zero_str (_LABEL_MAX);
   cs_globals->repel_path = new_zero_str (_PATH_MAX);
   cs_globals->cds_path = new_zero_str (_PATH_MAX);
   cs_globals->query_path = new_zero_str (_PATH_MAX);
   cs_globals->results_path = new_zero_str (_PATH_MAX);
   cs_globals->username = new_zero_str (_LABEL_MAX);
   cs_globals->analysis_mode = new_zero_str (_LABEL_MAX);
   cs_globals->roi_ref_path = new_zero_str (_PATH_MAX);
   cs_globals->roi_file_path = new_zero_str (_PATH_MAX);   // = _ROI_FILE_PATH;
   cs_globals->mpas_file_path = new_zero_str (_PATH_MAX);   // = _ROI_FILE_PATH;
   cs_globals->seqid = new_zero_str (_LABEL_MAX);
   cs_globals->current_ref_path = new_zero_str (_PATH_MAX);
   cs_globals->report_type = new_zero_str (_LABEL_MAX);
   strncpy(cs_globals->report_type, value_report_type_paf, strlen(value_report_type_paf));
   sprintf(cs_globals->jobname, "bolero_%s_%d",timestamp(0), getpid());
   cs_globals->n_threads = 8;
   cs_globals->flush = 0;
   cs_globals->max_edit_distance = 20;
   strncpy(cs_globals->analysis_mode, analysis_mode_gdna,strlen(analysis_mode_gdna));
   sprintf(cs_globals->username, "%s_prog", THIS_PROGRAM_NAME);
   strncpy(cs_globals->roi_file_path, _ROI_DEFAULT_FILE_PATH, PATH_MAX);
   call[0] = 0;
   sprintf(call, "mkdir -p ./.%s", THIS_PROGRAM_NAME);
   int ret = system(call);
   if (ret != 0) {
      fprintf(stderr, "Unable to create the roi file\n");
      exit (-1);
   }
   cs_globals->locoord = 0;
   cs_globals->hicoord = 0;
   cs_globals->extract_mode = extract_mode_extra;
   cs_globals->quiet_mode = 1;
   cs_globals->fivep_extra = 0;
   cs_globals->threep_extra = 0;
   cs_globals->roi_block_size = 0;
   cs_globals->step = 0;
   cs_globals->repel_mm_reader = NULL;
   cs_globals->cds_mm_reader = NULL;
   cs_globals->current_mm_reader = NULL;
   cs_globals->repel_mm_idx = NULL;
   cs_globals->cds_mm_idx = NULL;
   cs_globals->current_mm_idx = NULL;
   repel_seq = NULL;
   cds_seq = NULL;
   current_ref_seq = NULL;
   cs_globals->keep_roi_file = 0;
   cs_globals->show_bed = 1;
   cs_globals->show_rcigar = 1;
   cs_globals->show_cigar = 0;
   cs_globals->show_sampaf = 0;
   cs_globals->show_sortid = 0;
   cs_globals->show_gcplot = 0;
   cs_globals->show_onehot = 0;
   _alignments = (Alignment **) malloc_mem(sizeof(_alignments));
   assert (_alignments);
   cs_globals->last_aln_idx = 0;
   cs_globals->ngap_threshold = 0;
   cs_globals->store_file = stdout;
   cs_globals->show_progress = 10000;
   cs_globals->gzip_mpas = 0;
}
void breakdown () {
   // if (current_mm_reader)  mm_idx_reader_close(current_mm_reader); // close the index reader
   // if (repel_mm_reader) mm_idx_reader_close(repel_mm_reader);
   // if (cds_mm_reader) mm_idx_reader_close(cds_mm_reader);
   unload_seq_file (CMD_VALUE_ROI);
   if (cs_globals->keep_roi_file == 0) {
      remove (cs_globals->roi_file_path);
   } else {
      char new_name[_PATH_MAX];
      pid_t pid = getpid();
      sprintf(new_name, "%d_%s", pid, cs_globals->roi_file_path);
      rename(cs_globals->roi_file_path, new_name);
   }
   free_mem(cs_globals->mpas_file_path);
   free_mem(cs_globals->config_path);
   free_mem(cs_globals->jobname);
   free_mem(cs_globals->repel_path);
   free_mem(cs_globals->cds_path);
   free_mem(cs_globals->query_path);
   free_mem(cs_globals->results_path);
   free_mem(cs_globals->username);
   free_mem(cs_globals->analysis_mode);
   free_mem(cs_globals->roi_ref_path);
   free_mem(cs_globals->roi_file_path);   // = _ROI_FILE_PATH;
   free_mem(cs_globals->seqid);
   free_mem(cs_globals->current_ref_path);
   free_mem(cs_globals->report_type);
   int i;
   for (i=0;i<cs_globals->last_aln_idx;i++) {
      if (_alignments[i]->pattern) {
         free_mem (_alignments[i]->pattern);
      }
      if (_alignments[i]->hitseq) {
         free_mem (_alignments[i]->hitseq);
      }
      free_mem (_alignments[i]);
   }
   // delete the roi file
   // free up memory if a ref is open
   // free up memory if a roi ref is open
   // free any malloc's not already freed
}
int value_as_int (const char *value) {
   if (str_equals(value, "on")) return 1;
   if (str_equals(value, "yes")) return 1;
   if (str_equals(value, "1")) return 1;
   if (str_equals(value, "true")) return 1;
   if (str_equals(value, "off")) return 0;
   if (str_equals(value, "no")) return 0;
   if (str_equals(value, "0")) return 0;
   if (str_equals(value, "false")) return 0;
   return 0;
}
void process_kvp (char *key, char *value) {
   if (key[0] > 96 && key[0] < 123) {
      if (str_equals(key, key_job))           { strncpy(cs_globals->jobname, value, PATH_MAX);                return; }
      if (str_equals(key, key_threads))       { cs_globals->n_threads = atoi(value);               return; }
      if (str_equals(key, key_repel_file))    { if (file_exists(value))                strncpy(cs_globals->repel_path, value, PATH_MAX);       return; } 
      if (str_equals(key, key_cds_file))      { if (file_exists(value))                strncpy(cs_globals->cds_path, value, PATH_MAX);         return; }
      if (str_equals(key, key_roi_ref_file))  { if (file_exists(value))                strncpy(cs_globals->roi_ref_path, value, PATH_MAX);     return; }
      if (str_equals(key, key_ref_file))      { if (file_exists(value))                strncpy(cs_globals->current_ref_path, value, PATH_MAX); return; }
      if (str_equals(key, key_username))      { strncpy(cs_globals->username, value, PATH_MAX);               return; }
      if (str_equals(key, key_mode))          { strncpy(cs_globals->analysis_mode, value, PATH_MAX);          return; }
      if (str_equals(key, key_roi_seqid))     { strncpy(cs_globals->seqid, value, PATH_MAX);                  return; }
      if (str_equals(key, key_roi_lo))        { cs_globals->locoord = atoi(value);                 return; }
      if (str_equals(key, key_roi_hi))        { cs_globals->hicoord = atoi(value);                 return; }
      if (str_equals(key, key_fivep_extra))   { cs_globals->fivep_extra = atoi(value);             return; }
      if (str_equals(key, key_threep_extra))  { cs_globals->threep_extra = atoi(value);            return; }
      if (str_equals(key, key_roi_block_size)){ cs_globals->roi_block_size = atoi(value);          return; }
      if (str_equals(key, key_query_file))    { strncpy(cs_globals->query_path, value, PATH_MAX);             return; }
      if (str_equals(key, key_results_file))  { strncpy(cs_globals->results_path, value, PATH_MAX);           return; }
      if (str_equals(key, key_mpas_file_path)){ strncpy(cs_globals->mpas_file_path, value, PATH_MAX);         return; }
      if (str_equals(key, key_roi_file))      { strncpy(cs_globals->roi_file_path, value, PATH_MAX);          return; }
      if (str_equals(key, key_keep_roi_file)) { cs_globals->keep_roi_file = value_as_int(value);   return; }
      if (str_equals(key, key_quiet_mode))    { cs_globals->quiet_mode = value_as_int(value);      return; }
      if (str_equals(key, key_report_introns)){ cs_globals->report_introns = value_as_int(value);  return; }
      if (str_equals(key, key_flush))         { cs_globals->flush = value_as_int(value);           return; }
      if (str_equals(key, key_max_edit_dist)) { cs_globals->max_edit_distance = atoi(value);       return; }
      if (str_equals(key, key_show_bed))      { cs_globals->show_bed = value_as_int(value);        return; }
      if (str_equals(key, key_show_cigar))    { cs_globals->show_cigar = value_as_int(value);      return; }
      if (str_equals(key, key_show_rcigar))   { cs_globals->show_rcigar = value_as_int(value);     return; }
      if (str_equals(key, key_show_sampaf))   { cs_globals->show_sampaf = value_as_int(value);     return; }
      if (str_equals(key, key_show_sortid))   { cs_globals->show_sortid = value_as_int(value);     return; }
      if (str_equals(key, key_show_gcplot))   { cs_globals->show_gcplot = atoi(value);             return; }
      if (str_equals(key, key_ngap_threshold)){ cs_globals->ngap_threshold = atoi(value);          return; }
      if (str_equals(key, key_show_onehot))   { cs_globals->show_onehot = value_as_int(value);     return; }
   
   } else {
      fprintf(outfile, "%s\n", "All settings must start with a lower case alpha character");
      return;
   }
   fprintf(outfile, "k %s: v %s not recognized and is ignored\n", key, value);
}
void asleep(unsigned int microseconds) {
    clock_t goal = microseconds * (CLOCKS_PER_SEC / 1000000) + clock();
    while (goal > clock()) {}
}
void do_explative (char *left, char *right) {
   register int l;
   register int r;
   register int ll = strlen(left);
   register int lr = strlen(right);
   if (ll > 8) ll = 8;
   if (lr > 8) lr = 8;
   for (l=0;l<ll;l++) for (r=0;r<lr;r++) { 
      asleep(500);
      fprintf(stderr, "%c%c\r", left[l], right[l]);
      if (l % 7 == 0) fprintf(stderr, "\n");
   }
   fprintf (stderr, "\n");
}

void do_cmd (char *cmd, char *value) {
   if (str_equals(cmd, CMD_ALIGN)) {     // ALIGN:QUERY_FILE, ALIGN:ROIs     This aligns the qfile or the roi_file_path
      align(value);
      return;
   }
   if (str_equals(cmd, CMD_DATABASE)) {     // ALIGN:QUERY_FILE, ALIGN:ROIs     This aligns the qfile or the roi_file_path
      if (str_equals(value, CMD_VALUE_DATABASE_CREATEMPAS)) {
         if (cs_globals->gzip_mpas) {
            char gzfilename[PATH_MAX];
            gzfilename[0] = 0;
            sprintf(gzfilename, "%s.gz", cs_globals->mpas_file_path);
            
         } else {
            fprintf(stderr, "Opening %s\n", cs_globals->mpas_file_path);
            cs_globals->store_file = fopen(cs_globals->mpas_file_path, "w");
            assert(cs_globals->mpas_file_path);
            outfile = cs_globals->store_file;
            fprintf(stderr, "Opened%s\n", cs_globals->mpas_file_path);
         }
         return;
      }
      if (str_equals(value, CMD_VALUE_DATABASE_CLOSEMPAS)) {
         if (cs_globals->gzip_mpas) {

         } else {
            if (cs_globals->store_file)
               fclose (cs_globals->store_file);
            cs_globals->store_file = stdout;
         }
         return;
      }
/*      if (str_equals(value, CMD_VALUE_DATABASE_CREATEMPAS)) {
         fprintf(stderr, "Opening %s\n", cs_globals->mpas_file_path);
         cs_globals->store_file = fopen(cs_globals->mpas_file_path, "w");
         assert(cs_globals->mpas_file_path);
         outfile = cs_globals->store_file;
         fprintf(stderr, "Opened%s\n", cs_globals->mpas_file_path);
         return;
      }
      if (str_equals(value, CMD_VALUE_DATABASE_CLOSEMPAS)) {
         if (cs_globals->store_file)
            fclose (cs_globals->store_file);
         cs_globals->store_file = stdout;
         return;
      }*/
   }
   if (str_equals(cmd, CMD_ECHO)) {
      fprintf(outfile, "%s\n", value);     // simple prints somethings to outfile
      return;
   }
   if (str_equals(cmd, CMD_ANALYZE)) {
      if (str_equals(value, CMD_VALUE_NGAP)) {
         uint32_t count = count_ns(cs_globals->current_ref_path);
         fprintf(outfile, "\t%s has %u ngaps\n", cs_globals->current_ref_path, count);     // simple prints somethings to outfile
      }
      if (str_equals(value, CMD_VALUE_SETTINGS)) {
         diagnostics(); // burp up the settings overall
      }
      return;
   }
   fprintf(outfile, "Command %s: %s not recognized and is ignored\n", cmd, value);
}
//  This increments for each step taken
void print_progress(char *instruction) {
   fprintf(stderr, "%s", ".oOo.:");
   char *timestr = timestamp(1);
   fprintf(stderr, " '%s' [%s]\n", instruction, timestr);
   free (timestr);
}
#define scan_cmd_instruction_format "!%[^:]:%[^#]"
#define scan_cmd_only_format "!%[^#]"
#define scan_keyval_format "%[^=]=%[^#]"
#define scan_ascii_one_liner "<%[^#]|%[^<]> ";
int run_step  (char *line) {  // %[^:]: %[^#]
   char keycmd[_PATH_MAX];
   char value[_PATH_MAX];
   keycmd[0] = 0;
   value[0] = 0;
   cs_globals->step++;
   if (line[0] == '#') return 1;
   if (line[0] == '!') {
      if (sscanf(line, scan_cmd_instruction_format,keycmd, value) || sscanf(line, scan_cmd_only_format,keycmd) ) {
         do_cmd(keycmd, value);
         print_progress(value);
         return 1;
      } else if (sscanf(line, scan_cmd_instruction_format,keycmd, value)) {
         do_explative(keycmd , value);
      }  else {
         fprintf(stderr, "%s @ step %d [%s %s]\n", "Improper syntax for !<cmd>", cs_globals->step, keycmd, value);
         return 0;
      }
   } if (line[0] == '$') {
      // perl one liner that acts on the roi file
      // this is used to transfornm the ATG into NNN
   } else 
      if (sscanf(line, scan_keyval_format,keycmd, value)) {
         process_kvp(keycmd, value);     
         print_progress(value);
         return 1;
      }
   fprintf(stderr, "Step %d ignored\n", cs_globals->step);
   return 0;
}
void step_through_file (char *step_file) {
	FILE *stream;
	char s[_CALL_STR_MAX];
	
	stream = cs_fopen (step_file, _READ_ONLY);
	if (!stream) {
		fprintf(stderr, "Unable to open list file %s\n", step_file);
		exit (-1);
	}
   // prescan the file
	while (fgets(s, _CALL_STR_MAX, stream)) {
      chop_off_newline(s);
		if (s[0] > 96 && s[0] < 123) {
         run_step(s);
         fprintf(stderr, " %s\n", "ok");
      }
	}
   rewind (stream);
	while (fgets(s, _CALL_STR_MAX, stream)) {
      chop_off_newline(s);
		run_step(s);
	}
	fclose (stream);
}

int align (char *instruction) {
	mm_idxopt_t iopt;
	mm_mapopt_t mopt;
   register int hits = 0;
   register float edave = 0.0f;
   register int edsum = 0;
   int contigs = 0;
   char *path;
   char last_contig_hit_name[64];
   float identity_sum = 0.0f;
   gzFile roif;
   int cntr = 0; 
	mm_verbose = 2; // disable message output to stderr
	mm_set_opt(0, &iopt, &mopt);
   set_mm_opt_by_analysis_mode(cs_globals->analysis_mode, &iopt, &mopt);
	mopt.flag |= MM_F_CIGAR; // print alignment
	
   // open query file for reading; you may use your favorite FASTA/Q parser
   if ((strlen(cs_globals->query_path) > 0) && file_exists (cs_globals->query_path)) {
	   roif = gzopen(cs_globals->query_path, "r");
	   assert(roif);
   } else {
	   roif = gzopen(cs_globals->roi_file_path, "r");
	   assert(roif);
   }
	kseq_t *qryfasta = kseq_init(roif);
   // this requires this state to be set 'query_filer=$roi_file_path'

	// open index reader
	mm_idx_reader_t *r;
   if (str_equals(instruction, CMD_VALUE_REPEL)) {
	   cs_globals->repel_mm_reader = mm_idx_reader_open(cs_globals->repel_path, &iopt, 0);
      path = cs_globals->repel_path;
      r = cs_globals->repel_mm_reader;
   } else if (str_equals(instruction, CMD_VALUE_CDS)) {
	   cs_globals->cds_mm_reader = mm_idx_reader_open(cs_globals->cds_path, &iopt, 0);
      r = cs_globals->cds_mm_reader;
      path = cs_globals->cds_path;
   } else {
      if (cs_globals->extract_mode == extract_mode_alignment_extra || cs_globals->extract_mode == extract_mode_extra) {
         load_seq_file(CMD_VALUE_CURRENT_REF);
      }
	   cs_globals->current_mm_reader = mm_idx_reader_open(cs_globals->current_ref_path, &iopt, 0);
      r = cs_globals->current_mm_reader;
      path = cs_globals->current_ref_path;
   }
	mm_idx_t *mi;
	while ((mi = mm_idx_reader_read(r, cs_globals->n_threads)) != 0) { // traverse each part of the index
		mm_mapopt_update(&mopt, mi); // this sets the maximum minimizer occurrence; TODO: set a better default in mm_mapopt_init()!
		mm_tbuf_t *tbuf = mm_tbuf_init(); // thread buffer; for multi-threading, allocate one tbuf for each thread
		gzrewind(roif);
		kseq_rewind(qryfasta);
		while (kseq_read(qryfasta) >= 0) { // each kseq_read() call reads one query sequence
			mm_reg1_t *reg;
			int j, n_reg;
         int edit_distance = 0;
         edsum = 0;
         contigs = 0;
         identity_sum = 0.0f;
         last_contig_hit_name[0] = 0;
         reg = mm_map(mi, qryfasta->seq.l, qryfasta->seq.s, &n_reg, tbuf, &mopt, 0); // get all hits for the query
        
         fprintf (outfile, ">%s  ref=%s\n",qryfasta->name.s, path);
			if (n_reg != 0) { 
            hits = 0;
            for (j = 0; j < n_reg; ++j) { // traverse hits and print them out
               mm_reg1_t *r = &reg[j];
               edit_distance = r->blen - r->mlen + r->p->n_ambi;
               assert(r->p); // with MM_F_CIGAR, this should not be NULL
               char *paf = 0;
               int max_buf_len = 0; 
               cs_globals->current_global_hitid++;
               mm_gen_cs(NULL, &paf, &max_buf_len, mi, r, qryfasta->seq.s, 0);  // this generates the paf string
               edsum += edit_distance;
               if (edit_distance) {
                  float pct_of_qry = (float) r->mlen / (float) qryfasta->seq.l;
                  identity_sum += pct_of_qry * ((float) edit_distance / (float) r->mlen);
               }
               fprintf(outfile, "- HITNUM: %d hit_run_id=%ld\n", hits, cs_globals->current_global_hitid);
               fprintf(outfile, "   refhit: %s\n", mi->seq[r->rid].name);
               if (!str_equals(mi->seq[r->rid].name, last_contig_hit_name)) {
                  contigs++;
                  strncpy(last_contig_hit_name, mi->seq[r->rid].name, 64);
               }

               if (cs_globals->show_sampaf) {      // SAMPAF INFO
                  fprintf(outfile, "   uidpaf: ");
                  fprintf(outfile, "%s\t%s\t%zu\t%d\t%d\t%c\t",uuid_pool(),  qryfasta->name.s, qryfasta->seq.l, r->qs, r->qe, "+-"[r->rev]);
                  fprintf(outfile, "%s\t%d\t%d\t%d\t%d\t%d\t%d\tnm:i:%d\tcg:Z:", mi->seq[r->rid].name, mi->seq[r->rid].len, r->rs, r->re, r->mlen, r->blen, r->mapq, edit_distance);
                  fprintf(outfile, "%s\n", paf);
               } 

               char *topstr = cs2mpas_entry_pool (paf, 1, 0);
               char *botstr = cs2mpas_entry_pool (paf, 0, 0);
#define LEADER_CALLER 0
               char *leader_space = NULL;
               int leader_size = 0;
               leader_size = r->qs;
               leader_space = string_pool (r->qs + 1, 1, LEADER_CALLER);
               register int i;
               for (i=0;i<leader_size;i++) {
                  leader_space[i] = ' ';
                  leader_space[i+1] = 0;
               }
               char *strand_str = qryfasta->seq.s;
               if (r->rev) strand_str = revcomp_pool(qryfasta->seq.s); 
               

               fprintf(outfile, "   cdsseq: %s\n", strand_str);
               fprintf(outfile, "   refseq:%s %s\n", leader_space, topstr);
               fprintf(outfile, "   qedits:%s %s\n", leader_space, botstr);
               

               char *rcs = NULL;
               if (cs_globals->show_rcigar) {      // RCIGAR INFO
                  fprintf(outfile, "   rcigar: ");
                  rcs = rcigar_pool (topstr, botstr);
                  if (!r->rev) {         // oddly, the r->rev needs to be flipped
                     fprintf(outfile, "--> %s -->\n", rcs);
                  } else {
                     fprintf(outfile, "<-- %s <--\n", rcs);
                  }
               }
#define PATTERN_CALLER 1
/*               char *sortid;
               max_buf_len = 0; 
               char *pattern = string_pool (strlen(paf) + (PATH_MAX * 2) + strlen (paf), 1, PATTERN_CALLER);
               sprintf(pattern, "  hitid=%ld:ref=%s:start=%d:end=%d:ed=%d:paf=%s", cs_globals->current_global_hitid, mi->seq[r->rid].name, r->rs, r->re, edit_distance, paf);
               if (mm_gen_MD(NULL, &sortid, &max_buf_len, mi, r, qryfasta->seq.s)) {
                  if (cs_globals->show_sortid) {   // SORTID INFO
                     fprintf (outfile, "   sortid: ");
                     fprintf (outfile, "%s\n", sortid);
                  }
               }
*/
               char hitid_str[HITID_STR_MAX];
               hitid_str[0] = 0;
               snprintf(hitid_str, HITID_STR_MAX, "%-38s hit_num_%-6zu", qryfasta->name.s, cs_globals->current_global_hitid);
               //add_alignment (sortid, leader_space, botstr, edit_distance, hitid_str, r->rs, r->re, mi->seq[r->rid].name);
               
               if (cs_globals->show_bed) {         // BED INFO
                  fprintf(outfile, "   bedroi: ");
                  char *bedstr = bed_line_pool (mi->seq[r->rid].name, r->rs, r->re, cs_globals->current_global_hitid, 0, "+-"[r->rev]);
                  fprintf(outfile, "%s\n",bedstr);
               }
               
               if (cs_globals->show_onehot) {         // ONE HOT INFO
                    fprintf(outfile, "   onehot: ");
                    onehot_printer(topstr, botstr);
                    fprintf(outfile, "\n");
               }

               if (cs_globals->extract_mode != extract_mode_none) {
#define THREEP_OVERLAP 16
                     // fprintf(stderr, "%llu prom sieze\n", (sizeof (uint8_t) * (cs_globals->fivep_extra + THREEP_OVERLAP + 1)));
                     uint8_t *fpseq = (uint8_t*) malloc (sizeof (uint8_t) * (cs_globals->fivep_extra + THREEP_OVERLAP + 1));
                     uint64_t _loco;
                     uint64_t _end;
                     uint64_t overlapping_extra = cs_globals->fivep_extra + THREEP_OVERLAP;
                     if (r->rev) {
                        _loco = r->re - THREEP_OVERLAP;
                        _end = _loco + overlapping_extra;;
                     } else {
                        _loco = r->rs - cs_globals->fivep_extra;
                        _end = r->rs + THREEP_OVERLAP;
                     }
		              mm_idx_getseq2(mi, r->rev, r->rid, _loco, _end, fpseq);
		              //mm_idx_getseq2(mi, 0, r->rid, _loco, _end, fpseq);
#define PROM_CALLER 2
                    if (fpseq) {
                        char *promseq = string_pool(overlapping_extra + 1, 1, PROM_CALLER);
                        register int i;
                        for (i=0;i<overlapping_extra;i++) {
                           promseq[i] = "ACGTN"[fpseq[i]]; 
                           promseq[i+1] = 0;
                        }
                        fprintf(outfile, "   promsq: %s\n", promseq);
                        if (cs_globals->show_gcplot) {         // GC PLOT
                           fprintf(outfile, "   gcplot: ");
                           char *gcplot_str = gc_profile_pool(promseq, NULL, cs_globals->show_gcplot);
                           fprintf(outfile, "%s\n",gcplot_str);
                        }
                        free(fpseq);
                    }
                 }
                 hits++;
                 edsum += edit_distance;
                 free(r->p);
           }
           free(reg);
         } else {
            fprintf(outfile, "nohits: No Hits observed\n");
            free(reg);
            continue;
			}
         float cons_scr = 1.0f;
         if (hits == 0 || edsum == 0) 
            cons_scr = 1.0f;
         else {
            float divisor = 1.0f;
            divisor = 1.0 / hits;
            edave = (float) edsum / (float) hits;
            cons_scr = 1.0 - (divisor / edave);
         }
           
          fprintf (outfile, "- CONSERVATION_CODE: dups=%d:contigs=%d:edave=%f:csvscr=%f [%d]\n\n", hits, contigs, edave, cons_scr, cntr);
          if (cs_globals->current_global_hitid % 10000 == 0) {
            char report[120];
            report[0] = 0;
            snprintf(report, 120, "@ %lu in %s", cs_globals->current_global_hitid, qryfasta->name.s);
            if (cs_globals->show_progress)
               print_progress(report);
            fflush(stderr);
          }
		}
		mm_tbuf_destroy(tbuf);
	   mm_idx_destroy(mi);
	}

   if (cs_globals->flush) fflush (outfile);   // done on the block so as to make sure a block is not broken up
	mm_idx_reader_close(r); // close the index reader
   if (str_equals(instruction, CMD_VALUE_CURRENT_REF))
      if (cs_globals->extract_mode == extract_mode_alignment_extra || cs_globals->extract_mode == extract_mode_extra) {
	      unload_seq_file(CMD_VALUE_CURRENT_REF);
      }
	kseq_destroy(qryfasta); // close the query file
	gzclose(roif);
	return 1;
}
void setup_easy_call (int _argc, char **_argv) {

}
void show_help (int arg_cnt, char **this_argv) {
   int print_help = 0;

   if (arg_cnt == 1) {
      print_help = 1;
   } else {
      if (arg_cnt == 2)  {
         if (!strstr(this_argv[1], "-h")) {
            print_help = 1;
         }
      }
   }
   if (print_help) {
      printf ("Usage: %s <instructions.cstp>\n", this_argv[0]);
      printf ("   or \n");
      printf ("Usage: %s <CDS Fasta file> <Results Path> <reference genome 1> .. <reference genome n>\n", this_argv[0]);
      printf ("\t%s is designed to extract a region of interest from the ROI genome and\n", this_argv[0]);
      printf ("\tthen align that region to a repeat database, a coding sequence database and then\n");
      printf ("\tall of the reference genomes passed to it. \n");
      printf ("\t%s will then report on any hits to repeats/TE's, coding sequence and then\n", this_argv[0]);
      printf ("\tall hits to the other references passed to it. It will also report on all\n");
      printf ("\tduplications. What is reported is controlled by the edit distance value. \n");
      printf ("\tWhen run using command line options, the edit distance is fixed at 10.\n");
      printf ("\tIf an instructions file is used, that value can be adjusted per reference alignment.\n");
      printf ("\tPlease review details by calling  'man composer_tools' on how to create an instructions file.\n");
      exit (0);
   }
}
#define cstp_index 1
#define cds_query_path_index 1
#define results_path_index 2
#define start_of_references_index 3
int main (int argc, char *argv[]) {
   show_help(argc, argv);
   outfile = stdout;
   // int arg_idx = 1;
	mm_verbose = 2; // disable message output to stderr
   cs_globals = (Globals *) malloc (sizeof(Globals));
   assert (cs_globals);
   setup();
   char qry_name[PATH_MAX];
   char ref_name[PATH_MAX];
   char path[PATH_MAX];
   if (strstr(argv[cstp_index], ".cstp")) {  // a cstp file has all the step commands in it
      // open the file and pass, step by step 
      step_through_file(argv[1]);
   } else {
      cs_globals->flush = 0;
      cs_globals->n_threads = 4;
      safe_strcpy(cs_globals->query_path, argv[cds_query_path_index], strlen(argv[cds_query_path_index]));
      safe_strcpy(cs_globals->results_path, argv[results_path_index], strlen(argv[results_path_index]));
      file_exists(cs_globals->results_path);
      char *s = strrchr(argv[cds_query_path_index], '/');
      strcpy(qry_name, ++s);
      cs_globals->show_sampaf = 1;
      cs_globals->show_onehot = 1;
      cs_globals->fivep_extra = 3500;
      cs_globals->threep_extra = 0;
      cs_globals->show_gcplot = 17;
      safe_strcpy(cs_globals->analysis_mode, analysis_mode_splice, strlen(analysis_mode_splice));
      int i;
      for (i=start_of_references_index;i<argc;i++) {
         ref_name[0] = 0;
         path[0] = 0;
         char *s = strrchr(argv[i], '/');
         strcpy(ref_name, ++s);
         sprintf(cs_globals->mpas_file_path, "%s/%s_vs_%s.mpas", cs_globals->results_path, qry_name, ref_name);
         cs_globals->store_file = fopen(cs_globals->mpas_file_path, "w");
         assert(cs_globals->mpas_file_path);
         outfile = cs_globals->store_file;
         safe_strcpy(cs_globals->current_ref_path, argv[i], strlen(argv[i]));
         
         align(CMD_VALUE_CURRENT_REF);

         if (cs_globals->store_file)
            fclose (cs_globals->store_file);
         cs_globals->store_file = stdout; 
      }
   }
   // print_alignments();
   breakdown();
   free(cs_globals);
   return 0;
}

/*
static void (*vtable[4])(void) = { fn_0, fn_1, fn_2, fn_3 };

void dispatch(unsigned state) {
  if (state >= 4) abort();
  (*vtable[state])();
}

*/
