//   gcc -g -O2 example.c libminimap2.a -lz

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <zlib.h>
#include "minimap.h"
#include "kseq.h"
#include "mmpriv.h"
#include "composers.h"

#define _PF_DEBUG 1

KSEQ_INIT(gzFile, gzread)

kseq_t *roi_ref_seq;
gzFile roi_ref_gz;
kseq_t *query_seq;
gzFile query_gz;
kseq_t *repel_seq;
gzFile repel_gz;
kseq_t *cds_seq;
gzFile cds_gz;
kseq_t *current_ref_seq;

int is_null (void *ptr) { return (ptr == NULL); }
int str_empty (const char *str) { return (str[0] == 0); }
int file_exists (const char *path) { return (access(path, F_OK) == 0); }
int str_equals (const char *str1, const char *str2) { return (strcmp(str1, str2) == 0); }
char *new_zero_str(int size) { char *ns = (char *) malloc(size + 1); assert(ns); ns[0] = 0; return ns; } 
void make_snake_name(char *str) {
   int len = strlen(str);
   int i;
   for (i=0;i<len;i++) {
      switch (str[i]) {
         case '|':
         case '$':
         case '!':
         case '@':
         case '(':
         case ')':
         case '*':
         case '&':
         case ',':
         case '/':
         case '\\':
            str[i] = '_';
            break;
      }
   }
}
FILE *cs_fopen(const char * restrict path, int _MODE) {
   if (_MODE == _APPEND) return fopen(path,"a");
   if (_MODE == _REPLACE) return fopen(path,"w");
   if (_MODE == _READ_ONLY) return fopen(path,"r");
   return NULL;
   // this is a wrapper for future file locking code
}
void unload_seq_file (char *value) {
   if (str_equals(value, CMD_VALUE_ROI)) {
	   if (roi_ref_seq) kseq_destroy(roi_ref_seq); // close the query file
	   if (roi_ref_gz) gzclose(roi_ref_gz);
      roi_ref_seq = NULL;
      roi_ref_gz = NULL;
   }
   if (str_equals(value, CMD_VALUE_REPEL)) {
	   if (repel_seq) kseq_destroy(repel_seq); // close the query file
	   if (repel_gz) gzclose(repel_gz);
      repel_seq = NULL;
      repel_gz = NULL;
   }
   if (str_equals(value, CMD_VALUE_CDS)) {
	   if (cds_seq) kseq_destroy(cds_seq); // close the query file
	   if (cds_gz) gzclose(cds_gz);
      cds_seq = NULL;
      cds_gz = NULL;
   }
}
void load_seq_file (char *value) {
   if (str_equals(value, CMD_VALUE_ROI)) {
      if (str_empty(roi_ref_path) || str_empty(seqid)) {
         fprintf(stderr, "%s\n", "Roi ref path or SeqID  not set");
         assert(0);
      }
      if (roi_ref_seq) {
         if (!strstr(roi_ref_seq->name.s, roi_ref_path)) {
            unload_seq_file(value);        
         }
      }
      roi_ref_gz = gzopen(roi_ref_path, "r");
      assert(roi_ref_gz);
      roi_ref_seq = kseq_init(roi_ref_gz);
   }
   if (str_equals(value, CMD_VALUE_REPEL)) {
      if (str_empty(repel_path)) {
         fprintf(stderr, "%s\n", "Repel file not set");
         assert(0);
      }
      if (repel_seq) {
         if (!strstr(repel_seq->name.s, repel_path)) {
            unload_seq_file(value);        
         }
      }
      repel_gz = gzopen(repel_path, "r");
      assert(repel_gz);
      repel_seq = kseq_init(repel_gz);
   }
   if (str_equals(value, CMD_VALUE_CDS)) {
      if (str_empty(cds_path)) {
         fprintf(stderr, "%s\n", "CDS path not set");
         assert(0);
      }
      if (cds_seq) {
         if (!strstr(cds_seq->name.s, cds_path)) {
            unload_seq_file(value);        
         }
      }
      cds_gz = gzopen(cds_path, "r");
      assert(cds_gz);
      cds_seq = kseq_init(cds_gz);
   }
}
int has_newlines(char *dna) {
   int cnt;
   int i;
   int len = strlen(dna);
   for (cnt=0, i=0;i<len;i++) {
      if (dna[i] == '\n') cnt++;
   }
   return cnt;
}
void extract_roi (int append ) {
   int extract_len = hicoord - locoord;
   char *extract_seq = NULL;
   char *newdefline = NULL;

   load_seq_file (CMD_VALUE_ROI);
   if (!roi_ref_gz || !roi_ref_seq) return;
   gzrewind(roi_ref_gz);
   kseq_rewind(roi_ref_seq);
   if (extract_len < 10) {
		fprintf(stderr, "You must extract more than 10 base pairs\n");
		assert(0);
   } 
   
   while (kseq_read(roi_ref_seq) >= 0) {
      if (str_equals(seqid, roi_ref_seq->name.s)) {
         extract_seq = (char *) malloc (extract_len + 1);
         if (extract_seq) {
            extract_seq[0] = 0;
            if (locoord > roi_ref_seq->seq.l || hicoord > roi_ref_seq->seq.l) {
               if (locoord < roi_ref_seq->seq.l && hicoord > roi_ref_seq->seq.l) {
                  hicoord = roi_ref_seq->seq.l;
               }
		         fprintf(stderr, "Coordinates are out of range for this sequence entry\n");
            }
            char *src_str_offset = roi_ref_seq->seq.s + locoord;
            strncpy(extract_seq, src_str_offset, extract_len);
            newdefline = (char *) malloc (strlen(roi_ref_seq->name.s) + extract_len + 300);
            newdefline[0] = 0;
            sprintf(newdefline, ">%s_%llu_%llu", roi_ref_seq->name.s, locoord, hicoord);
            make_snake_name(newdefline);
            FILE *ef = cs_fopen (roi_file_path, append);
            assert (ef);
            fprintf(ef, "%s\n",newdefline);
            fprintf(ef, "%s\n",extract_seq);
            printf ("# ### Found %d newlines\n",has_newlines(extract_seq)); 
            fclose (ef);
            free (newdefline);
            free (extract_seq);
         }
      }
   }
}

void setup ( ) {
   char call[_PATH_MAX];
   config_path = new_zero_str (_CALL_STR_MAX);
   jobname = new_zero_str (_LABEL_MAX);
   repel_path = new_zero_str (_PATH_MAX);
   cds_path = new_zero_str (_PATH_MAX);
   query_path = new_zero_str (_PATH_MAX);
   results_path = new_zero_str (_PATH_MAX);
   username = new_zero_str (_LABEL_MAX);
   analysis_mode = new_zero_str (_LABEL_MAX);
   roi_ref_path = new_zero_str (_PATH_MAX);
   roi_file_path = new_zero_str (_PATH_MAX);   // = _ROI_FILE_PATH;
   seqid = new_zero_str (_LABEL_MAX);
   current_ref_path = new_zero_str (_PATH_MAX);
   strcpy(jobname, "sbarberj");
   n_threads = 2;
   flush = 0;
   strcpy(analysis_mode, analysis_mode_gdna);
   strcpy(username, "sbarber");
   strcpy(roi_file_path, _ROI_DEFAULT_FILE_PATH);
   call[0] = 0;
   sprintf(call, "mkdir -p %s", "./.barber");
   int ret = system(call);
   if (ret != 0) {
      fprintf(stderr, "Unable to create the roi file\n");
      exit (-1);
   }
   locoord = 0;
   hicoord = 0;
   extract_mode = 0;
   quiet_mode = 1;
   fivep_extra = 0;
   threep_extra = 0;
   roi_block_size = 0;
   block_step = 0;
   repel_mm_reader = NULL;
   cds_mm_reader = NULL;
   current_mm_reader = NULL;
   repel_mm_idx = NULL;
   cds_mm_idx = NULL;
   current_mm_idx = NULL;
   repel_seq = NULL;
   cds_seq = NULL;
   current_ref_seq = NULL;
   keep_roi_file = 0;
   mm_verbose = 2; // disable message output to stderr
}
void breakdown () {
   // if (current_mm_reader)  mm_idx_reader_close(current_mm_reader); // close the index reader
   // if (repel_mm_reader) mm_idx_reader_close(repel_mm_reader);
   // if (cds_mm_reader) mm_idx_reader_close(cds_mm_reader);
   unload_seq_file (CMD_VALUE_ROI);
   if (keep_roi_file == 0) {
      remove (roi_file_path);
   } else {
      char new_name[_PATH_MAX];
      pid_t pid = getpid();
      sprintf(new_name, "%d_%s", pid, roi_file_path);
      rename(roi_file_path, new_name);
   }
   free(config_path);
   free(jobname);
   free(repel_path);
   free(cds_path);
   free(query_path);
   free(results_path);
   free(username);
   free(analysis_mode);
   free(roi_ref_path);
   free(roi_file_path);   // = _ROI_FILE_PATH;
   free(seqid);
   free(current_ref_path);
   // delete the roi file
   // free up memory if a ref is open
   // free up memory if a roi ref is open
   // free any malloc's not already freed
}
int value_as_int (const char *value) {
   if (str_equals(value, "on")) return 1;
   if (str_equals(value, "yes")) return 1;
   if (str_equals(value, "1")) return 1;
   if (str_equals(value, "true")) return 1;
   if (str_equals(value, "off")) return 0;
   if (str_equals(value, "no")) return 0;
   if (str_equals(value, "0")) return 0;
   if (str_equals(value, "false")) return 0;
   return 0;
}
void process_kvp (char *key, char *value) {
   if (key[0] > 96 && key[0] < 123) {
      if (str_equals(key, key_job))           
         { strcpy(jobname, value); return; }
      if (str_equals(key, key_threads))       
         { n_threads = atoi(value); return; }
      if (str_equals(key, key_repel_file))    
         { if (file_exists(value)) 
            strcpy(repel_path, value); return; }
      if (str_equals(key, key_cds_file))      
         { if (file_exists(value)) 
            strcpy(cds_path, value); return; }
      if (str_equals(key, key_roi_ref_file))  
         { if (file_exists(value)) 
            strcpy(roi_ref_path, value); return; }
      if (str_equals(key, key_ref_file))      
         { if (file_exists(value)) 
            strcpy(current_ref_path, value); return; }
      if (str_equals(key, key_username))      
         { strcpy(username, value); return; }
      if (str_equals(key, key_mode))          
         { strcpy(analysis_mode, value); return; }
      if (str_equals(key, key_roi_seqid))     
         { strcpy(seqid, value); return; }
      if (str_equals(key, key_roi_lo))        
         { locoord = atoi(value); return; }
      if (str_equals(key, key_roi_hi))        
         { hicoord = atoi(value); return; }
      if (str_equals(key, key_fivep_extra))   
         { fivep_extra = atoi(value); return; }
      if (str_equals(key, key_threep_extra))  
         { threep_extra = atoi(value); return; }
      if (str_equals(key, key_roi_block_size))
         { roi_block_size = atoi(value); return; }
      if (str_equals(key, key_query_file))    
         { strcpy(query_path, value); return; }
      if (str_equals(key, key_results_file))  
         { strcpy(results_path, value); return; }
      if (str_equals(key, key_roi_file))      
         { strcpy(roi_file_path, value); return; }
      if (str_equals(key, key_keep_roi_file)) 
         { keep_roi_file = value_as_int(value); return; }
      if (str_equals(key, key_extract_mode))  
         { extract_mode = value_as_int(value); return; }
      if (str_equals(key, key_quiet_mode))  
         { quiet_mode = value_as_int(value); return; }
      if (str_equals(key, key_report_introns))
         { report_introns = value_as_int(value); return; }
      if (str_equals(key, key_flush))         
         { flush = value_as_int(value); return; }
   } else {
      fprintf(stdout, "%s\n", "All settings must start with a lower case alpha character");
      return;
   }
   fprintf(stdout, "k %s: v %s not recognized and is ignored\n", key, value);
}

void do_cmd (char *cmd, char *value) {
   if (str_equals(cmd, CMD_SAVE)) {       // SAVE:ROI saves the last ROI settings to the indicatied ROI file
      if (str_equals(value, CMD_VALUE_ROI)) {
         extract_roi (_APPEND);        // This allows for repeated blocks to be saved 
      }
      return;
   }
   if (str_equals(cmd, CMD_ALIGN)) {     // ALIGN:QUERY_FILE, ALIGN:ROIs     This aligns the qfile or the roi_file_path
      align(value);
      return;
   }
   if (str_equals(cmd, CMD_EXTRACT)) {     // ALIGN:QUERY_FILE, ALIGN:ROIs     This aligns the qfile or the roi_file_path
      extract_roi(_APPEND);
      return;
   }
   if (str_equals(cmd, CMD_ECHO)) {
      fprintf(stdout, "%s\n", value);     // simple prints somethings to stdout
      return;
   }
   fprintf(stdout, "Command %s: v %s not recognized and is ignored\n", cmd, value);
}
//  This increments for each step taken
void print_progress(size_t count, size_t max, int bar_width, char *c) {
    if (quiet_mode) return;
    float progress = (float) count / max;
    int bar_length = progress * bar_width;

    fprintf(stderr, "\rSteps completed: [");
    for (int i = 0; i < bar_length; ++i) {
        fprintf(stderr, "%s", c);
    }
    for (int i = bar_length; i < bar_width; ++i) {
        fprintf(stderr, " ");
    }
    fprintf(stderr, "] %.2f%%", progress * 100);

    fflush(stderr);
}
#define scan_cmd_instruction_format "!%[^:]:%[^#]"
#define scan_cmd_only_format "!%[^#]"
#define scan_keyval_format "%[^=]=%[^#]"
#define scan_perl_one_linter "$%[^#]";
int run_step  (char *line) {  // %[^:]: %[^#]
   char keycmd[_PATH_MAX];
   char value[_PATH_MAX];
   keycmd[0] = 0;
   value[0] = 0;
   step++;
   if (line[0] == '#') return 1;
   if (line[0] == '!') {
      if (sscanf(line, scan_cmd_instruction_format,keycmd, value) || sscanf(line, scan_cmd_only_format,keycmd) ) {
         do_cmd(keycmd, value);
         return 1;
      } else {
         fprintf(stderr, "%s @ step %d [%s %s]\n", "Improper syntax for !<cmd>", step, keycmd, value);
         return 0;
      }
   } if (line[0] == '$') {
      // perl one liner that acts on the roi file
      // this is used to transfornm the ATG into NNN
   } else 
      if (sscanf(line, scan_keyval_format,keycmd, value)) {
         process_kvp(keycmd, value);     
         return 1;
      }
   fprintf(stderr, "Step %d ignored\n", step);
   return 0;
}
void step_through_file (char *step_file) {
	FILE *stream;
	char s[_CALL_STR_MAX];
	
	stream = cs_fopen (step_file, _READ_ONLY);
	if (!stream) {
		fprintf(stderr, "Unable to open list file %s\n", step_file);
		exit (-1);
	}
	while (fgets(s, _CALL_STR_MAX, stream)) {
		run_step(s);
	}
	fclose (stream);
}

int align (char *instruction) {
	mm_idxopt_t iopt;
	mm_mapopt_t mopt;
	int n_threads = 3;
   char *path;

	mm_verbose = 2; // disable message output to stderr
	mm_set_opt(0, &iopt, &mopt);
	mopt.flag |= MM_F_CIGAR; // print alignment

	// open query file for reading; you may use your favorite FASTA/Q parser
	gzFile f = gzopen(roi_file_path, "r");
	assert(f);
	kseq_t *ks = kseq_init(f);

	// open index reader
	mm_idx_reader_t *r;
   if (str_equals(instruction, CMD_VALUE_REPEL)) {
	   repel_mm_reader = mm_idx_reader_open(repel_path, &iopt, 0);
      path = repel_path;
      r = repel_mm_reader;
   } else if (str_equals(instruction, CMD_VALUE_CDS)) {
	   cds_mm_reader = mm_idx_reader_open(cds_path, &iopt, 0);
      r = cds_mm_reader;
      path = cds_path;
   } else {
	   current_mm_reader = mm_idx_reader_open(current_ref_path, &iopt, 0);
      r = current_mm_reader;
      path = current_ref_path;
   }
	mm_idx_t *mi;
   void *km = NULL;
   int max_len = 0;
	while ((mi = mm_idx_reader_read(r, n_threads)) != 0) { // traverse each part of the index
		mm_mapopt_update(&mopt, mi); // this sets the maximum minimizer occurrence; TODO: set a better default in mm_mapopt_init()!
		mm_tbuf_t *tbuf = mm_tbuf_init(); // thread buffer; for multi-threading, allocate one tbuf for each thread
		gzrewind(f);
		kseq_rewind(ks);
		while (kseq_read(ks) >= 0) { // each kseq_read() call reads one query sequence
			mm_reg1_t *reg;
			int j, n_reg;
			reg = mm_map(mi, ks->seq.l, ks->seq.s, &n_reg, tbuf, &mopt, 0); // get all hits for the query
			for (j = 0; j < n_reg; ++j) { // traverse hits and print them out
				mm_reg1_t *r = &reg[j];
				assert(r->p); // with MM_F_CIGAR, this should not be NULL
				printf("%s\t%zu\t%d\t%d\t%c\t", ks->name.s, ks->seq.l, r->qs, r->qe, "+-"[r->rev]);
				printf("%s\t%d\t%d\t%d\t%d\t%d\t%d\tcg:Z:", mi->seq[r->rid].name, mi->seq[r->rid].len, r->rs, r->re, r->mlen, r->blen, r->mapq);
            char *pstr = (char *) calloc(0,0);
            mm_gen_cs(km, &pstr, &max_len, mi, r, ks->seq.s, 0);  // this generates the paf string
            printf("%s", pstr);
				free(pstr);
            putchar('\n');
				free(r->p);
			}
         if (n_reg == 0) {
           printf("No hits found in %s", path); 
         }
			free(reg);
		}
		mm_tbuf_destroy(tbuf);
		mm_idx_destroy(mi);
	}
	mm_idx_reader_close(r); // close the index reader
	kseq_destroy(ks); // close the query file
	gzclose(f);
	return 1;
}

int main (int argc, char *argv[], char *env[] ) {
   int arg_idx = 1;
	mm_verbose = 2; // disable message output to stderr
   setup();
   if (strstr(argv[1], ".cstp")) {  // a cstp file has all the step commands in it
      // open the file and pass, step by step 
      step_through_file(argv[1]);
   } else {
      char *instruction = new_zero_str(_PATH_MAX);
      sprintf(instruction, "%s=%s", key_roi_ref_file, argv[arg_idx++]);    
         run_step(instruction);
      sprintf(instruction, "%s=%s", key_roi_seqid, argv[arg_idx++]);       
         run_step(instruction);
      sprintf(instruction, "%s=%s", key_roi_lo, argv[arg_idx++]);          
         run_step(instruction);
      sprintf(instruction, "%s=%s", key_roi_hi, argv[arg_idx++]);          
         run_step(instruction);
      sprintf(instruction, "%s=%s", key_repel_file, argv[arg_idx++]);      
         run_step(instruction);
      sprintf(instruction, "%s=%s", key_cds_file, argv[arg_idx++]);        
         run_step(instruction);
      print_progress(arg_idx, argc, argc, "#");
      sprintf(instruction, "!%s:%s", CMD_SAVE,CMD_VALUE_ROI);              
         run_step(instruction);
      sprintf(instruction, "!%s:%s", CMD_ALIGN,CMD_VALUE_REPEL);           
         run_step(instruction);
      print_progress(arg_idx, argc, argc, "#");
      sprintf(instruction, "!%s:%s", CMD_ALIGN,CMD_VALUE_CDS);             
         run_step(instruction);
      print_progress(arg_idx, argc, argc, "#");
      
      for(;arg_idx < argc;arg_idx++) {
         sprintf(instruction, "%s=%s", key_ref_file, argv[arg_idx]);       
            run_step(instruction);
         sprintf(instruction, "%s=%s", key_mode, analysis_mode_hapblock);  
            run_step(instruction);
         sprintf(instruction, "!%s:%s", CMD_ALIGN,CMD_VALUE_CURRENT_REF);  
            run_step(instruction);
         print_progress(arg_idx, argc, argc, "#");
       }

      free(instruction);
   }
   breakdown();
}  
