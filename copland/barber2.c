//head and put the Rudy card on file if you want to pay for it To compile:
//   gcc -g -O2 example.c libminimap2.a -lz

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <math.h>
#include <zlib.h>
#include "minimap.h"
#include "kseq.h"
#include "mmpriv.h"
#include "composers.h"
#include <pthread.h>

#define _PF_DEBUG 1
#define MAX_HIT_LEN_DIVISOR 10000.00
#define ASCII_CODE_0 48
#define THIS_PROGRAM_NAME "barber"
#define TESTMODE 1
#define DO_PRINTF 1
char *prog_str = ".-^-";
int progidx = 0;
int allocations = 0;
int alignment_index = 0;
int last_aln_idx = 0;
unsigned long current_global_hitid = 0;
pthread_mutex_t lock;
KSEQ_INIT(gzFile, gzread)

kseq_t *roi_seq;
gzFile roi_gz;
kseq_t *query_seq;
gzFile query_gz;
kseq_t *repel_seq;
gzFile repel_gz;
kseq_t *cds_seq;
gzFile cds_gz;
kseq_t *current_ref_seq;
gzFile current_ref_gz;

int is_null (void *ptr) { return (ptr == NULL); }
int str_empty (const char *str) { return (str[0] == 0); }
int str_equals (const char *str1, const char *str2) { return (strcmp(str1, str2) == 0); }
char *new_zero_str(int size) { char *ns = (char *) malloc(size + 1); assert(ns); ns[0] = 0; allocations++; return ns; } 
void free_mem (void *ptr) { if (ptr) { free (ptr); allocations--; }}
void *malloc_mem(uint64_t size) { void *p = malloc(size); if (p) allocations++; return p; }
void chop_off_newline(char *s) { register size_t i;register size_t len = strlen(s); for(i=0;i<len;i++) if (s[i] == '\n') { s[i] = 0; return; } }
#define _SZ 32768
void informative_abort(const char *format, const char *info1, const char *info2) {
      fprintf (stderr, format, info1, info2);
      fprintf (stdout, format, info1, info2);
      fflush(stdout);
      assert(0); 
}
int file_exists (const char *path) { 
   if (access(path, F_OK) != 0) 
      informative_abort("File %s does not exist [%s]\n",path, "");
   return 1; 
}
uint64_t mm_idx_refsize(const mm_idx_t *mi) {
    uint32_t i;
    uint64_t len = 0;
    for (i = 0; i < mi->n_seq; ++i)
        len += mi->seq[i].len;
    return len;
}

char *timestamp (void) {
    char ts[PATH_MAX];
    ts[0] = 0;
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(ts, "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    return strdup(ts);
}
float get_sortf(int len, const char *str) {
   unsigned long hash = 5381;
   register int c;
   while ((c = *str++)) hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
   unsigned long modulo_hash = hash % _SZ;
   float sortf = (float) len + (100.00 / (float) modulo_hash);
   return sortf;
}
int set_mm_opt_by_analysis_mode(char  *mode, mm_idxopt_t *io, mm_mapopt_t *mo) {
    if (str_equals(mode,analysis_mode_none)) {
        // if (io) mm_idxopt_init_cs (io);
        // if (mo) mm_mapopt_init_cs (mo);
    } else if (str_equals(mode, analysis_mode_ava_ont)) {
        if (io) io->flag = 0;
        if (io) io->k = 15;
        if (io) io->w = 5;
        if (mo) mo->flag |= MM_F_ALL_CHAINS | MM_F_NO_DIAG | MM_F_NO_DUAL | MM_F_NO_LJOIN | MM_F_CIGAR | MM_F_OUT_MD;
        if (mo) mo->min_chain_score = 100; if (mo) mo->pri_ratio = 0.0f; if (mo) mo->max_gap = 10000; if (mo) mo->max_chain_skip = 25;
        if (mo) mo->bw = 2000;
    } else if (str_equals(mode,  analysis_mode_asm5) || str_equals(mode, analysis_mode_hapblock)) {
        if (io) io->flag = 0;
        if (io) io->k = 19;
        if (io) io->w = 19;
        if (mo) mo->a = 1;
        if (mo) mo->b = 19;
        if (mo) mo->q = 39;
        if (mo) mo->q2 = 81;
        if (mo) mo->e = 3;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 200;
        if (mo) mo->min_mid_occ = 100;
        if (mo) mo->min_dp_max = 200;
        if (mo) mo->best_n = 50;
    } else if (str_equals(mode, analysis_mode_asm10)) {
        if (io) io->flag = 0;
        if (io) io->k = 19;
        if (io) io->w = 19;
        if (mo) mo->a = 1;
        if (mo) mo->b = 9;
        if (mo) mo->q = 16;
        if (mo) mo->q2 = 41;
        if (mo) mo->e = 2;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 200;
        if (mo) mo->min_mid_occ = 100;
        if (mo) mo->min_dp_max = 200;
        if (mo) mo->best_n = 50;
    } else if (str_equals(mode,analysis_mode_asm20)) {
        if (io) io->flag = 0;
        if (io) io->k = 19;
        if (io) io->w = 10;
        if (mo) mo->a = 1;
        if (mo) mo->b = 4;
        if (mo) mo->q = 6;
        if (mo) mo->q2 = 26;
        if (mo) mo->e = 2;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 200;
        if (mo) mo->min_mid_occ = 100;
        if (mo) mo->min_dp_max = 200;
        if (mo) mo->best_n = 50;
    } else if (str_equals(mode,analysis_mode_acr)
               || str_equals( mode , analysis_mode_sr)
               ) {
        if (io) io->flag = 0;
        if (io) io->k = 13;
        if (io) io->w = 8;
//        if (io) io->k = 15;
//        if (io) io->w = 5;
        if (mo) mo->flag |= MM_F_SR
                | MM_F_FRAG_MODE
                | MM_F_2_IO_THREADS
                | MM_F_HEAP_SORT
                | MM_F_OUT_CS
                | MM_F_CIGAR
                | MM_F_OUT_MD;
        if (mo) mo->pe_ori = 0<<1|1; // FR
        if (mo) mo->a = 2;
        if (mo) mo->b = 8;
        if (mo) mo->q = 12;
        if (mo) mo->e = 2;
        if (mo) mo->q2 = 24;
        if (mo) mo->e2 = 1;
        if (mo) mo->zdrop = mo->zdrop_inv = 100;
        if (mo) mo->end_bonus = 10;
        if (mo) mo->max_frag_len = 800;
        if (mo) mo->max_gap = 100;
        if (mo) mo->bw = 100;
        if (mo) mo->pri_ratio = 0.5f;
        if (mo) mo->min_cnt = 2;
        if (mo) mo->min_chain_score = 25;
        if (mo) mo->min_dp_max = 40;
        if (mo) mo->best_n = 20;
        if (mo) mo->mid_occ = 1000;
        if (mo) mo->max_occ = 5000;
        if (io) io->bucket_bits = 14;
        if (io) io->batch_size = 4000000000ULL;
        if (mo) mo->mini_batch_size = 50000000;
        // if (str_equals(mode, analysis_mode_skim_map))
        //     if (mo) mo->sc_ambi = 1;
    } else if (str_equals( mode, analysis_mode_cds) 
               || str_equals(mode, analysis_mode_splice)
               || str_equals(mode, analysis_mode_splice_hq)) {
        if (io) io->flag = 0;
        if (io) io->k = 15;
        if (io) io->w = 5;
        if (mo) mo->flag |= MM_F_SPLICE
                        | MM_F_SPLICE_FOR
                        | MM_F_SPLICE_REV
                        | MM_F_SPLICE_FLANK
                        | MM_F_CIGAR
                        | MM_F_OUT_MD;
        if (mo) mo->max_gap = 2000;
        if (mo) mo->max_gap_ref = mo->bw = 200000;
        if (mo) mo->a = 1;      //match score
        if (mo) mo->b = 2;      // mismatch score
        if (mo) mo->q = 2;
        if (mo) mo->e = 1;
        if (mo) mo->q2 = 32;
        if (mo) mo->e2 = 0;
        if (mo) mo->noncan = 9;
        if (mo) mo->junc_bonus = 9;
        if (mo) mo->zdrop = 200;
        if (mo) mo->zdrop_inv = 100; // because mo->a is halved
        if (str_equals(mode, analysis_mode_splice_hq)) {
            if (mo) mo->junc_bonus = 5;
            if (mo) mo->b = 4;
            if (mo) mo->q = 6;
            if (mo) mo->q2 = 24;
        }
    } else return -1;
    return 0;
}
char *gc_profile_alloc (char *_topstr, char *_botstr, uint32_t window_size) {
   int len = strlen(_topstr);
   char *gc_plot = new_zero_str(len);
   int i,j;
   int A,C,G,T,N;
   int _half = window_size / 2;
   for (i=0;i<len;i++) {
      for (A=0,C=0,G=0,T=0,N=0,j=i-_half;j<i+_half;j++) {
         if (j >=0 && j < len) {
            if (_botstr[j] == '.' || _botstr == NULL) {
               if (_topstr[j] == 'A') { A++; continue; }
               if (_topstr[j] == 'C') { C++; continue; }
               if (_topstr[j] == 'G') { G++; continue; }
               if (_topstr[j] == 'T') { T++; continue; }
            } else {
               if (_botstr[j] == 'a') { A++; continue; }
               if (_botstr[j] == 'c') { C++; continue; }
               if (_botstr[j] == 'g') { G++; continue; }
               if (_botstr[j] == 't') { T++; continue; }
            }
         }
      }
      float gc_ave = (float) (G + C) / (A + T + G + C);
      long int gc_int = lrintf(gc_ave * 10.0);
      gc_plot[i] = (char) gc_int + ASCII_CODE_0;
   }
   gc_plot[i+1] = 0;
   return gc_plot;
}
Alignment *new_alignment_alloc( ) {
   Alignment *this_aln = (Alignment *) malloc_mem ( sizeof(Alignment) );
   assert (this_aln);
   this_aln->sortf = 0.0;
   this_aln->ed = 0;
   this_aln->hitid = NULL;
   this_aln->pattern = NULL;
   this_aln->leader = NULL;
   allocations++;
   return this_aln;
}
Alignment **add_alignment (char *_leader_space, char *_pattern,  unsigned long _ed, char * _hitid, uint64_t _lo, uint64_t _hi, char *_hitseq) { 
   Alignment **na = realloc (alignments, sizeof(Alignment) * (last_aln_idx + 1));
   assert (na);
   if (na != alignments) alignments = na;
   Alignment *a  = new_alignment_alloc();
   a->ed = _ed;
   a->sortf = get_sortf(strlen(_pattern), _pattern); 
   a->hitid = new_zero_str(strlen (_hitid));
   strncpy(a->hitid, _hitid, strlen(_hitid));
   a->leader = new_zero_str(strlen (_leader_space));
   strncpy(a->leader, _leader_space, strlen(_leader_space));
   a->pattern = new_zero_str(strlen (_pattern));
   strncpy(a->pattern, _pattern, strlen(_pattern));
   a->hitseq = new_zero_str(strlen(_hitseq));
   strncpy(a->hitseq, _hitseq, strlen(_hitseq));

   alignments[last_aln_idx] = a;
   last_aln_idx++;
   return na;
}
void sort() {
    int i, j;
    Alignment *this_aln = NULL;
    for (i = 1; i < last_aln_idx; i++) {
        this_aln = alignments[i];
        j = i - 1;
        while (j >= 0 && alignments[j]->sortf > this_aln->sortf) {
            alignments[j + 1] = alignments[j];
            j = j - 1;
        }
        alignments[j + 1] = this_aln;
    }
}
#define scan_ref_name_format "%s_vs_%s_REF_"
void print_alignments() { 
   float last_sortf = 0.00;
   int sort_rank = 0;
   size_t i;
   char *timestr;

   timestr = timestamp();
   fprintf(stderr, "%s %s\n", "Sorting...", timestr);
   free (timestr);
   sort();
   timestr = timestamp();
   fprintf(stderr, "%s %s\ni\n", "...Complete", timestr);
   printf ("%6s,", "GROUP");
   printf ("%32s,%30s,",  "QUERY ID", "HIT NUM");
   printf ("%s,'%s'\n", "REF HIT", "ALIGN PATTERN");
   free (timestr);
   for (i=0;i<last_aln_idx;i++) {
      if (alignments[i]->hitid[0] != 0 && alignments[i]->pattern)  {
      	if (alignments[i]->sortf != last_sortf) {
     	      sort_rank++;
     	   }
         char refn[PATH_MAX]; refn[0] = 0;
         char *p1 = strstr(alignments[i]->hitseq, "_vs_");
         if (p1) {
            p1 += 4;
            char *p2 = strstr(p1, "_REF_");
            int len = p2 - p1;
            strncpy(refn, p1, len);
            refn[len + 1] = 0;
         }
     	   printf ("%6d,", sort_rank);
     	   printf ("%32s,%30s,",  alignments[i]->hitid, refn);
     	   printf ("'%s%s',", alignments[i]->leader, alignments[i]->pattern);
     	   printf ("[%f]\n", alignments[i]->sortf);
     	   last_sortf = alignments[i]->sortf;
      }
   }
}
uint32_t count_ns(char* file) {
    if (cs_globals->ngap_threshold < 9) return -1;
    FILE *f = fopen(file, "r");
    char buf[_SZ];
    uint32_t cnt = 0;
    int run_sz = 0;
    for(;;) {
        size_t res = fread(buf, 1, _SZ, f);
        if (ferror(f)) return -1;
        int i;
        for(i = 1; i < res; i++) {
            if (buf[i] == 'N') {
               if (run_sz > cs_globals->ngap_threshold) continue;
               if ( buf[i - 1] == 'N') {
                  run_sz++;
               }
            } else {
               run_sz = 0;
            }
            if (run_sz == cs_globals->ngap_threshold) 
               cnt++;
        }
        if (feof(f)) break;
    }
    fclose (f);
    return cnt;
}

void make_snake_name(char *str) {
   register int len = strlen(str);
   register int i;
   for (i=0;i<len;i++) {
      switch (str[i]) {
         case '|':
         case '$':
         case '!':
         case '@':
         case '(':
         case ')':
         case '*':
         case '&':
         case ',':
         case '/':
         case '\\':
            str[i] = '_';
            break;
      }
   }
}
FILE *cs_fopen(const char * restrict path, int _MODE) {
   if (_MODE == _APPEND) return fopen(path,"a");
   if (_MODE == _REPLACE) return fopen(path,"w");
   if (_MODE == _READ_ONLY) return fopen(path,"r");
   return NULL;
   // this is a wrapper for future file locking code
}
void unload_seq_file (char *value) {
   if (str_equals(value, CMD_VALUE_CURRENT_REF)) {
	   if (current_ref_seq) kseq_destroy(current_ref_seq); // close the query file
	   if (current_ref_gz) gzclose(current_ref_gz);
      current_ref_seq = NULL;
      current_ref_gz = NULL;
   }
   if (str_equals(value, CMD_VALUE_ROI)) {
	   if (roi_seq) kseq_destroy(roi_seq); // close the query file
	   if (roi_gz) gzclose(roi_gz);
      roi_seq = NULL;
      roi_gz = NULL;
   }
   if (str_equals(value, CMD_VALUE_REPEL)) {
	   if (repel_seq) kseq_destroy(repel_seq); // close the query file
	   if (repel_gz) gzclose(repel_gz);
      repel_seq = NULL;
      repel_gz = NULL;
   }
   if (str_equals(value, CMD_VALUE_CDS)) {
	   if (cds_seq) kseq_destroy(cds_seq); // close the query file
	   if (cds_gz) gzclose(cds_gz);
      cds_seq = NULL;
      cds_gz = NULL;
   }
}
void load_seq_file (char *value) {
   if (str_equals(value, CMD_VALUE_CURRENT_REF)) {
      if (str_empty(cs_globals->current_ref_path)) {
         informative_abort("%s [%s]\n", "current_ref_path not set", "");
      }
      current_ref_gz = gzopen(cs_globals->current_ref_path, "r");
      assert(current_ref_gz);
      current_ref_seq = kseq_init(current_ref_gz);
   }
   if (str_equals(value, CMD_VALUE_ROI)) {
      if (str_empty(cs_globals->roi_ref_path) || str_empty(cs_globals->seqid)) {
         informative_abort("Roi ref path [%s] or SeqID [%s] not set\n", cs_globals->roi_ref_path, cs_globals->seqid);
      }
      if (roi_seq) {
         if (!strstr(roi_seq->name.s, cs_globals->roi_ref_path)) {
            unload_seq_file(value);        
         }
      }
      roi_gz = gzopen(cs_globals->roi_ref_path, "r");
      assert(roi_gz);
      roi_seq = kseq_init(roi_gz);
   }
   if (str_equals(value, CMD_VALUE_REPEL)) {
      if (str_empty(cs_globals->repel_path)) {
         informative_abort("Repel file [%s] not set [%s]\n", cs_globals->repel_path, "");
      }
      if (repel_seq) {
         if (!strstr(repel_seq->name.s, cs_globals->repel_path)) {
            unload_seq_file(value);        
         }
      }
      repel_gz = gzopen(cs_globals->repel_path, "r");
      assert(repel_gz);
      repel_seq = kseq_init(repel_gz);
   }
   if (str_equals(value, CMD_VALUE_CDS)) {
      if (str_empty(cs_globals->cds_path)) {
         informative_abort("CDS path[%s] not set [%s]\n", cs_globals->cds_path, "");
      }
      if (cds_seq) {
         if (!strstr(cds_seq->name.s, cs_globals->cds_path)) {
            unload_seq_file(value);        
         }
      }
      cds_gz = gzopen(cs_globals->cds_path, "r");
      assert(cds_gz);
      cds_seq = kseq_init(cds_gz);
   }
}
int has_newlines(char *dna) {
   int cnt;
   int i;
   int len = strlen(dna);
   for (cnt=0, i=0;i<len;i++) {
      if (dna[i] == '\n') cnt++;
   }
   return cnt;
}
void compliment (char *dna) {
    register unsigned long len = strlen(dna);
    register unsigned long i;
    for (i=0;i<len;i++) {
        switch (dna[i]) {
            case 'A':
                dna[i] = 'T';
                break;
            case 'a':
                dna[i] = 't';
                break;
            case 'C':
                dna[i] = 'G';
                break;
            case 'c':
                dna[i] = 'g';
                break;
            case 'G':
                dna[i] = 'C';
                break;
            case 'g':
                dna[i] = 'c';
                break;
            case 'T':
                dna[i] = 'A';
                break;
            case 't':
                dna[i] = 'a';
                break;
            case '.':
                dna[i] = '.';
                break;
            case 'N':
                dna[i] = 'N';
                break;
            case 'n':
                dna[i] = 'n';
                break;
            default:
                             
                break;
        }
    }
}
char *revcomp_alloc (char *instr) {
    register long len = strlen(instr);
    char *outstr = (char *) malloc(len + 1);
    outstr[0] = 0;
    register long i = len;
    register long j = 0;
    for (i=len;i>=0 && j < len;j++, i--) {
        switch (instr[i -1]) {
            case 'A':
                outstr[j] = 'T';
                break;
            case 'a':
                outstr[j] = 't';
                break;
            case 'C':
                outstr[j] = 'G';
                break;
            case 'c':
                outstr[j] = 'g';
                break;
            case 'G':
                outstr[j] = 'C';
                break;
            case 'g':
                outstr[j] = 'c';
                break;
            case 'T':
                outstr[j] = 'A';
                break;
            case 't':
                outstr[j] = 'a';
                break;
            case '.':
                outstr[j] = '.';
                break;
            case 'N':
                outstr[j] = 'N';
                break;
            case 'n':
                outstr[j] = 'n';
                break;
            default:
                outstr[j] = instr[i - 1];
                break;
        }
    }
    outstr[j] = 0;
    return outstr;
}
char *cs2mpas_entry_alloc (const char *cs_str, int top) {
    int cs_mode = 0;
    register unsigned long slen = strlen(cs_str) + 1;
    register unsigned long i,j;
    char rstr[slen];
    char astr[slen];
    rstr[0] = 0;
    astr[0] = 0;
    for (i=0, j=0;i < slen && j < slen;i++) {
        switch (cs_str[i]) {
            case '=': cs_mode = 1; continue; break;
            case ':': cs_mode = 2; continue; break;
            case '*': cs_mode = 3; continue; break;
            case '+': cs_mode = 4; continue; break;
            case '-': cs_mode = 5; continue; break;
            case '~': cs_mode = 6; continue; break;
        }
        switch (cs_mode) {
            case 1:
                rstr[j] = cs_str[i];
                astr[j] = '.';
                rstr[++j] = 0;
                astr[j] = 0;
                break;
            case 2:
                rstr[j] = cs_str[i];
                astr[j] = '.';
                rstr[++j] = 0;
                astr[j] = 0;
                break;
            case 3:
                rstr[j] = cs_str[i++];
                astr[j] = cs_str[i];
                rstr[++j] = 0;
                astr[j] = 0;
                break;
            case 4:
                rstr[j] = cs_str[i];
                astr[j] = '-';
                rstr[++j] = 0;
                astr[j] = 0;
                break;
            case 5:
                rstr[j] = '-';
                astr[j] = cs_str[i];
                rstr[++j] = 0;
                astr[j] = 0;
                break;
            case 6:
                rstr[j] = cs_str[i];
                astr[j] = '~';
                rstr[++j] = 0;
                astr[j] = 0;
                break;
            default:
                break;
        }
    }
    astr[strlen(rstr)] = 0;
    char *s = NULL;
    if (top > 0) {
       s = strdup(rstr);
    } else {
       s = strdup(astr);
    }
    return s;
}
char *bed_line_alloc (char *chrom, int start, int end, int hit_num, double score, char strand) {
    char *beds = new_zero_str(_LABEL_MAX);
    sprintf (beds, "%s\t%d\t%d\thit_num_%d\t%f\t%c", chrom, start, end + 1, hit_num, score, strand);
    return beds;
}
char *rcigar_alloc (char *refstr, char *qrystr) {
   register int len = strlen(qrystr);
   char *rcig_str = new_zero_str(len);
   register int i,j;
   for (i=0,j=0;i<len;i++) {
     if (qrystr[i] == '.') {
        if (i == 0)
           rcig_str[j++] = '.', rcig_str[j] = 0;
        else if (qrystr[i - 1] != '.')
           rcig_str[j++] = '.', rcig_str[j] = 0;
        continue;
     } 
     if ((qrystr[i] == 'a' && refstr[i] == 'g') || (qrystr[i] == 'g' && refstr[i] == 'a') || (qrystr[i] == 'c' && refstr[i] == 't') || (qrystr[i] == 't' && refstr[i] == 'c')) {
         rcig_str[j++] = 'X', rcig_str[j] = 0;
         continue;
     } else  {
         rcig_str[j++] = 'x', rcig_str[j] = 0;     // transversion
         continue;
     }
     if (refstr[i] == '-' && qrystr[i] != '.') {
         if ((i - 1) > -1 && refstr[i - 1] == '-')  continue;
         rcig_str[j++] = 'D', rcig_str[j] = 0;
         continue;
     }
     if (qrystr[i] == '-' && refstr[i] != '-') {
         if (qrystr[i - 1] == '-')  continue;
         rcig_str[j++] = 'I',  rcig_str[j] = 0;
         continue;
     }
   }
   return rcig_str;
}
void extract_roi (char *value, char *seqname , char *queryname) {
   int extract_len;
   char *extract_seq = NULL;
   char *newdefline = NULL;
   unsigned long long _locoord;
   unsigned long long _hicoord;
   // load_seq_file (CMD_VALUE_ROI);
   // if (!roi_gz || !roi_seq) return;
   gzFile this_gz;
   kseq_t *this_seq;

   extract_len = cs_globals->hicoord - cs_globals->locoord;
   if (str_equals(value, CMD_VALUE_CURRENT_REF) || str_equals(value, CMD_VALUE_EXTRACT_HIT)) { 
     this_gz = current_ref_gz;
     this_seq = current_ref_seq;
   }
   if (str_equals(value, CMD_VALUE_ROI)) {
     this_gz = roi_gz;
     this_seq = roi_seq;
   }
   if (str_equals(value, CMD_VALUE_REPEL)) {
     this_gz = repel_gz;
     this_seq = repel_seq;
   }
   if (str_equals(value, CMD_VALUE_CDS)) {
     this_gz = cds_gz;
     this_seq = cds_seq;
   }

   gzrewind(this_gz);
   kseq_rewind(this_seq);
   if (seqname) strcpy(cs_globals->seqid, seqname);
   while (kseq_read(this_seq) >= 0) {
      if (str_equals(cs_globals->seqid, this_seq->name.s)) {
          _locoord = cs_globals->locoord;
          _hicoord = cs_globals->hicoord;
          if (_locoord > this_seq->seq.l || _hicoord > this_seq->seq.l) {
             if (_locoord < this_seq->seq.l && _hicoord > this_seq->seq.l) {
                _hicoord = this_seq->seq.l;
             }
             fprintf(stderr, "Coordinates %ld %ld are out of range for this sequence entry. [contig: %s, size %ld]\n", _locoord, _hicoord, this_seq->name.s, this_seq->seq.l);
          }
          extract_len = _hicoord - _locoord;
          extract_seq = new_zero_str (extract_len + 1);
          char *src_str_offset = this_seq->seq.s + _locoord;
          strncpy(extract_seq, src_str_offset, extract_len);
          newdefline = new_zero_str (strlen(this_seq->name.s) + extract_len + 300);
          if (seqname) {
             sprintf(newdefline, ">%s_%llu_%llu = promoter for %s", seqname, _locoord, _hicoord, queryname);
          } else {
             sprintf(newdefline, ">%s_%llu_%llu", this_seq->name.s, _locoord, _hicoord);
          } 
          make_snake_name(newdefline);
          FILE *ef;
          if (str_equals(value, CMD_VALUE_EXTRACT_HIT)) {
             ef = cs_fopen (extracted_path, _APPEND);
          } else {
             ef = cs_fopen (cs_globals->roi_file_path, _REPLACE);
          }
          assert (ef);
          fprintf(ef, "%s\n",newdefline);
          fprintf(ef, "%s\n",extract_seq);
          fclose (ef);
          free_mem (newdefline);
          free_mem (extract_seq);
      }
   }
}
#define format_string "# ## %24s: %s\n"
#define format_number "# ## %24s: %d\n"
void diagnostics () {
      fprintf(stdout,  format_string, key_job, cs_globals->jobname);                      fprintf(stdout,  format_number, key_threads,cs_globals->n_threads);                    fprintf(stdout,  format_string, key_repel_file, cs_globals->repel_path);        
      fprintf(stdout,  format_string, key_cds_file, cs_globals->cds_path);                fprintf(stdout,  format_string, key_roi_ref_file, cs_globals->roi_ref_path);           fprintf(stdout,  format_string, key_ref_file, cs_globals->current_ref_path); 
      fprintf(stdout,  format_string, key_username, cs_globals->username);                fprintf(stdout,  format_string, key_mode, cs_globals->analysis_mode);                  fprintf(stdout,  format_string, key_roi_seqid, cs_globals->seqid);                  
      fprintf(stdout,  format_number, key_roi_lo, (int) cs_globals->locoord);             fprintf(stdout,  format_number, key_roi_hi, (int) cs_globals->hicoord);                fprintf(stdout,  format_number, key_fivep_extra, (int) cs_globals->fivep_extra);             
      fprintf(stdout,  format_number, key_threep_extra, (int) cs_globals->threep_extra);  fprintf(stdout,  format_number, key_roi_block_size, (int) cs_globals->roi_block_size); fprintf(stdout,  format_string, key_query_file, cs_globals->query_path);             
      fprintf(stdout,  format_string, key_results_file, cs_globals->results_path);        fprintf(stdout,  format_string, key_extracted_path, extracted_path);       fprintf(stdout,  format_string, key_roi_file, cs_globals->roi_file_path);          
      fprintf(stdout,  format_number, key_keep_roi_file, (int) cs_globals->keep_roi_file);fprintf(stdout,  format_number, key_extract_mode, (int) cs_globals->extract_mode);     fprintf(stdout,  format_number, key_quiet_mode, cs_globals->quiet_mode);      
      fprintf(stdout,  format_number, key_report_introns, cs_globals->report_introns);    fprintf(stdout,  format_number, key_flush, cs_globals->flush);                         fprintf(stdout,  format_number, key_max_edit_dist, (int) cs_globals->max_edit_distance);       
      fprintf(stdout,  format_number, key_show_bed, cs_globals->show_bed);                fprintf(stdout,  format_number, key_show_cigar, cs_globals->show_cigar);               fprintf(stdout,  format_number, key_show_rcigar, cs_globals->show_rcigar);     
      fprintf(stdout,  format_number, key_show_sampaf, cs_globals->show_sampaf);          fprintf(stdout,  format_number, key_show_sortid, cs_globals->show_sortid);             fprintf(stdout,  format_number, key_show_gcplot, cs_globals->show_gcplot);     
      fprintf(stdout,  format_number, key_ngap_threshold, cs_globals->ngap_threshold);    fprintf(stdout,  format_number, key_show_onehot, cs_globals->show_onehot);     
}
void setup ( ) {
   char call[_PATH_MAX];
   extracted_path = new_zero_str (_PATH_MAX);
   cs_globals->config_path = new_zero_str (_CALL_STR_MAX);
   cs_globals->jobname = new_zero_str (_LABEL_MAX);
   cs_globals->repel_path = new_zero_str (_PATH_MAX);
   cs_globals->cds_path = new_zero_str (_PATH_MAX);
   cs_globals->query_path = new_zero_str (_PATH_MAX);
   cs_globals->results_path = new_zero_str (_PATH_MAX);
   cs_globals->username = new_zero_str (_LABEL_MAX);
   cs_globals->analysis_mode = new_zero_str (_LABEL_MAX);
   cs_globals->roi_ref_path = new_zero_str (_PATH_MAX);
   cs_globals->roi_file_path = new_zero_str (_PATH_MAX);   // = _ROI_FILE_PATH;
   cs_globals->seqid = new_zero_str (_LABEL_MAX);
   cs_globals->current_ref_path = new_zero_str (_PATH_MAX);
   cs_globals->report_type = new_zero_str (_LABEL_MAX);
   strcpy(cs_globals->report_type, value_report_type_paf);
   sprintf(cs_globals->jobname, "sbarberj_%d", getpid());
   cs_globals->n_threads = 8;
   cs_globals->flush = 0;
   cs_globals->max_edit_distance = 20;
   strcpy(cs_globals->analysis_mode, analysis_mode_gdna);
   sprintf(cs_globals->username, "%s_prog", THIS_PROGRAM_NAME);
   strcpy(cs_globals->roi_file_path, _ROI_DEFAULT_FILE_PATH);
   call[0] = 0;
   sprintf(call, "mkdir -p ./.%s", THIS_PROGRAM_NAME);
   int ret = system(call);
   if (ret != 0) {
      fprintf(stderr, "Unable to create the roi file\n");
      exit (-1);
   }
   cs_globals->locoord = 0;
   cs_globals->hicoord = 0;
   cs_globals->extract_mode = 0;
   cs_globals->quiet_mode = 1;
   cs_globals->fivep_extra = 0;
   cs_globals->threep_extra = 0;
   cs_globals->roi_block_size = 0;
   cs_globals->step = 0;
   cs_globals->repel_mm_reader = NULL;
   cs_globals->cds_mm_reader = NULL;
   cs_globals->current_mm_reader = NULL;
   cs_globals->repel_mm_idx = NULL;
   cs_globals->cds_mm_idx = NULL;
   cs_globals->current_mm_idx = NULL;
   repel_seq = NULL;
   cds_seq = NULL;
   current_ref_seq = NULL;
   cs_globals->keep_roi_file = 0;
   cs_globals->show_bed = 1;
   cs_globals->show_rcigar = 1;
   cs_globals->show_cigar = 0;
   cs_globals->show_sampaf = 1;
   cs_globals->show_sortid = 0;
   cs_globals->show_gcplot = 0;
   cs_globals->show_onehot = 0;
   alignments = (Alignment **) malloc_mem(sizeof(alignments));
   assert (alignments);
   last_aln_idx = 0;
   allocations++;
   cs_globals->ngap_threshold = 0;
   mm_verbose = 2; // disable message output to stderr
}
void breakdown () {
   // if (current_mm_reader)  mm_idx_reader_close(current_mm_reader); // close the index reader
   // if (repel_mm_reader) mm_idx_reader_close(repel_mm_reader);
   // if (cds_mm_reader) mm_idx_reader_close(cds_mm_reader);
   unload_seq_file (CMD_VALUE_ROI);
   if (cs_globals->keep_roi_file == 0) {
      remove (cs_globals->roi_file_path);
   } else {
      char new_name[_PATH_MAX];
      pid_t pid = getpid();
      sprintf(new_name, "%d_%s", pid, cs_globals->roi_file_path);
      rename(cs_globals->roi_file_path, new_name);
   }
   free_mem(extracted_path);
   free_mem(cs_globals->config_path);
   free_mem(cs_globals->jobname);
   free_mem(cs_globals->repel_path);
   free_mem(cs_globals->cds_path);
   free_mem(cs_globals->query_path);
   free_mem(cs_globals->results_path);
   free_mem(cs_globals->username);
   free_mem(cs_globals->analysis_mode);
   free_mem(cs_globals->roi_ref_path);
   free_mem(cs_globals->roi_file_path);   // = _ROI_FILE_PATH;
   free_mem(cs_globals->seqid);
   free_mem(cs_globals->current_ref_path);
   free_mem(cs_globals->report_type);
   int i;
   for (i=0;i<last_aln_idx;i++) {
      if (alignments[i]->pattern) {
         free_mem (alignments[i]->pattern);
      }
      if (alignments[i]->leader) {
         free_mem (alignments[i]->leader);
      }
      if (alignments[i]->hitid) {
         free_mem (alignments[i]->hitid);
      }
      if (alignments[i]->hitseq) {
         free_mem (alignments[i]->hitseq);
      }
      free_mem (alignments[i]);
   }
   // delete the roi file
   // free up memory if a ref is open
   // free up memory if a roi ref is open
   // free any malloc's not already freed
}
int mode_as_int (const char *value) {
   if (str_equals(value, value_extract_mode_alignment)) return extract_mode_alignment;
   if (str_equals(value, value_extract_mode_alignment_extra)) return extract_mode_alignment_extra;
   if (str_equals(value, value_extract_mode_extra)) return extract_mode_extra;
   return extract_mode_none;
}
int value_as_int (const char *value) {
   if (str_equals(value, "on")) return 1;
   if (str_equals(value, "yes")) return 1;
   if (str_equals(value, "1")) return 1;
   if (str_equals(value, "true")) return 1;
   if (str_equals(value, "off")) return 0;
   if (str_equals(value, "no")) return 0;
   if (str_equals(value, "0")) return 0;
   if (str_equals(value, "false")) return 0;
   return 0;
}
void process_kvp (char *key, char *value) {
   if (key[0] > 96 && key[0] < 123) {
      if (str_equals(key, key_job))           { strcpy(cs_globals->jobname, value);                return; }
      if (str_equals(key, key_threads))       { cs_globals->n_threads = atoi(value);               return; }
      if (str_equals(key, key_repel_file))    { if (file_exists(value))                strcpy(cs_globals->repel_path, value);       return; } 
      if (str_equals(key, key_cds_file))      { if (file_exists(value))                strcpy(cs_globals->cds_path, value);         return; }
      if (str_equals(key, key_roi_ref_file))  { if (file_exists(value))                strcpy(cs_globals->roi_ref_path, value);     return; }
      if (str_equals(key, key_ref_file))      { if (file_exists(value))                strcpy(cs_globals->current_ref_path, value); return; }
      if (str_equals(key, key_username))      { strcpy(cs_globals->username, value);               return; }
      if (str_equals(key, key_mode))          { strcpy(cs_globals->analysis_mode, value);          return; }
      if (str_equals(key, key_roi_seqid))     { strcpy(cs_globals->seqid, value);                  return; }
      if (str_equals(key, key_roi_lo))        { cs_globals->locoord = atoi(value);                 return; }
      if (str_equals(key, key_roi_hi))        { cs_globals->hicoord = atoi(value);                 return; }
      if (str_equals(key, key_fivep_extra))   { cs_globals->fivep_extra = atoi(value);             return; }
      if (str_equals(key, key_threep_extra))  { cs_globals->threep_extra = atoi(value);            return; }
      if (str_equals(key, key_roi_block_size)){ cs_globals->roi_block_size = atoi(value);          return; }
      if (str_equals(key, key_query_file))    { strcpy(cs_globals->query_path, value);             return; }
      if (str_equals(key, key_results_file))  { strcpy(cs_globals->results_path, value);           return; }
      if (str_equals(key, key_extracted_path)){ strcpy(extracted_path, value);         return; }
      if (str_equals(key, key_roi_file))      { strcpy(cs_globals->roi_file_path, value);          return; }
      if (str_equals(key, key_keep_roi_file)) { cs_globals->keep_roi_file = value_as_int(value);   return; }
      if (str_equals(key, key_extract_mode))  { cs_globals->extract_mode = mode_as_int(value);     return; }
      if (str_equals(key, key_quiet_mode))    { cs_globals->quiet_mode = value_as_int(value);      return; }
      if (str_equals(key, key_report_introns)){ cs_globals->report_introns = value_as_int(value);  return; }
      if (str_equals(key, key_flush))         { cs_globals->flush = value_as_int(value);           return; }
      if (str_equals(key, key_max_edit_dist)) { cs_globals->max_edit_distance = atoi(value);       return; }
      if (str_equals(key, key_show_bed))      { cs_globals->show_bed = value_as_int(value);        return; }
      if (str_equals(key, key_show_cigar))    { cs_globals->show_cigar = value_as_int(value);      return; }
      if (str_equals(key, key_show_rcigar))   { cs_globals->show_rcigar = value_as_int(value);     return; }
      if (str_equals(key, key_show_sampaf))   { cs_globals->show_sampaf = value_as_int(value);     return; }
      if (str_equals(key, key_show_sortid))   { cs_globals->show_sortid = value_as_int(value);     return; }
      if (str_equals(key, key_show_gcplot))   { cs_globals->show_gcplot = value_as_int(value);     return; }
      if (str_equals(key, key_ngap_threshold)){ cs_globals->ngap_threshold = atoi(value);          return; }
      if (str_equals(key, key_show_onehot))   { cs_globals->show_onehot = value_as_int(value);     return; }
   } else {
      fprintf(stdout, "%s\n", "All settings must start with a lower case alpha character");
      return;
   }
   fprintf(stdout, "k %s: v %s not recognized and is ignored\n", key, value);
}

void do_cmd (char *cmd, char *value) {
   if (str_equals(cmd, CMD_SAVE)) {       // SAVE:ROI saves the last ROI settings to the indicatied ROI file
      if (str_equals(value, CMD_VALUE_ROI)) {
         load_seq_file (CMD_VALUE_ROI);
         extract_roi (value, NULL, NULL);        // This allows for repeated blocks to be saved 
         unload_seq_file(CMD_VALUE_ROI);
      }
      return;
   }
   if (str_equals(cmd, CMD_ALIGN)) {     // ALIGN:QUERY_FILE, ALIGN:ROIs     This aligns the qfile or the roi_file_path
      align(value);
      return;
   }
   if (str_equals(cmd, CMD_EXTRACT)) {     // ALIGN:QUERY_FILE, ALIGN:ROIs     This aligns the qfile or the roi_file_path
      // extract_roi(value, NULL);  //
      return;
   }
   if (str_equals(cmd, CMD_ECHO)) {
      fprintf(stdout, "%s\n", value);     // simple prints somethings to stdout
      return;
   }
   if (str_equals(cmd, CMD_ANALYZE)) {
      if (str_equals(value, CMD_VALUE_NGAP)) {
         uint32_t count = count_ns(cs_globals->current_ref_path);
         fprintf(stdout, "\t%s has %u ngaps\n", cs_globals->current_ref_path, count);     // simple prints somethings to stdout
      }
      if (str_equals(value, CMD_VALUE_SETTINGS)) {
         diagnostics(); // burp up the settings overall
      }
      return;
   }
   fprintf(stdout, "Command %s: %s not recognized and is ignored\n", cmd, value);
}
//  This increments for each step taken
void print_progress(char *instruction) {
   fprintf(stderr, "%c", prog_str[progidx++]);
   if (progidx > 3)  {
      progidx = 0;
   }
   char *timestr = timestamp();
   fprintf(stderr, " '%s' [%s] [%lu]\n", instruction, timestr, current_global_hitid);
   free (timestr);
}
#define scan_cmd_instruction_format "!%[^:]:%[^#]"
#define scan_cmd_only_format "!%[^#]"
#define scan_keyval_format "%[^=]=%[^#]"
#define scan_perl_one_liner "$%[^#]";
int run_step  (char *line) {  // %[^:]: %[^#]
   char keycmd[_PATH_MAX];
   char value[_PATH_MAX];
   keycmd[0] = 0;
   value[0] = 0;
   cs_globals->step++;
   if (line[0] == '#') return 1;
   if (line[0] == '!') {
      if (sscanf(line, scan_cmd_instruction_format,keycmd, value) || sscanf(line, scan_cmd_only_format,keycmd) ) {
         do_cmd(keycmd, value);
         print_progress(value);
         return 1;
      } else {
         fprintf(stderr, "%s @ step %d [%s %s]\n", "Improper syntax for !<cmd>", cs_globals->step, keycmd, value);
         return 0;
      }
   } if (line[0] == '$') {
      // perl one liner that acts on the roi file
      // this is used to transfornm the ATG into NNN
   } else 
      if (sscanf(line, scan_keyval_format,keycmd, value)) {
         process_kvp(keycmd, value);     
         print_progress(value);
         return 1;
      }
   fprintf(stderr, "Step %d ignored\n", cs_globals->step);
   return 0;
}
void step_through_file (char *step_file) {
	FILE *stream;
	char s[_CALL_STR_MAX];
	
	stream = cs_fopen (step_file, _READ_ONLY);
	if (!stream) {
		fprintf(stderr, "Unable to open list file %s\n", step_file);
		exit (-1);
	}
   // prescan the file
	while (fgets(s, _CALL_STR_MAX, stream)) {
      chop_off_newline(s);
		if (s[0] > 96 && s[0] < 123) {
         run_step(s);
         fprintf(stderr, " %s\n", "ok");
      }
	}
   rewind (stream);
	while (fgets(s, _CALL_STR_MAX, stream)) {
      chop_off_newline(s);
		run_step(s);
	}
	fclose (stream);
}

int align (char *instruction) {
	mm_idxopt_t iopt;
	mm_mapopt_t mopt;
   int hit_counter = 0;
   int hits = 0;
   float edave = 0.0f;
   int edsum = 0;
   int contigs = 0;
   char *path;
   char last_contig_hit_name[64];
   float identity_sum = 0.0f;
   gzFile roif;
   double eval = 0.0; 
	mm_verbose = 2; // disable message output to stderr
	mm_set_opt(0, &iopt, &mopt);
   set_mm_opt_by_analysis_mode(cs_globals->analysis_mode, &iopt, &mopt);
	mopt.flag |= MM_F_CIGAR; // print alignment
   mopt.flag |= MM_F_NO_PRINT_2ND; // no secondary stuff
   // open query file for reading; you may use your favorite FASTA/Q parser
   if ((strlen(cs_globals->query_path) > 0) && file_exists (cs_globals->query_path)) {
	   roif = gzopen(cs_globals->query_path, "r");
	   assert(roif);
   } else {
	   roif = gzopen(cs_globals->roi_file_path, "r");
	   assert(roif);
   }
	kseq_t *qryfasta = kseq_init(roif);
   // this requires this state to be set 'query_filer=$roi_file_path'

	// open index reader
	mm_idx_reader_t *r;
   if (str_equals(instruction, CMD_VALUE_REPEL)) {
	   cs_globals->repel_mm_reader = mm_idx_reader_open(cs_globals->repel_path, &iopt, 0);
      path = cs_globals->repel_path;
      r = cs_globals->repel_mm_reader;
   } else if (str_equals(instruction, CMD_VALUE_CDS)) {
	   cs_globals->cds_mm_reader = mm_idx_reader_open(cs_globals->cds_path, &iopt, 0);
      r = cs_globals->cds_mm_reader;
      path = cs_globals->cds_path;
   } else {
      if (cs_globals->extract_mode == extract_mode_alignment_extra || cs_globals->extract_mode == extract_mode_extra) {
         load_seq_file(CMD_VALUE_CURRENT_REF);
      }
	   cs_globals->current_mm_reader = mm_idx_reader_open(cs_globals->current_ref_path, &iopt, 0);
      r = cs_globals->current_mm_reader;
      path = cs_globals->current_ref_path;
   }
	mm_idx_t *mi;
	while ((mi = mm_idx_reader_read(r, cs_globals->n_threads)) != 0) { // traverse each part of the index
		mm_mapopt_update(&mopt, mi); // this sets the maximum minimizer occurrence; TODO: set a better default in mm_mapopt_init()!
		mm_tbuf_t *tbuf = mm_tbuf_init(); // thread buffer; for multi-threading, allocate one tbuf for each thread
		gzrewind(roif);
		kseq_rewind(qryfasta);
      //db_length = (unsigned long long) mm_idx_refsize(mi);	
		while (kseq_read(qryfasta) >= 0) { // each kseq_read() call reads one query sequence
			mm_reg1_t *reg;
			int j, n_reg;
         int edit_distance = 0;
         int alignment_score = 0;
         edsum = 0;
         contigs = 0;
         hits = 0;
         last_contig_hit_name[0] = 0;
         reg = mm_map(mi, qryfasta->seq.l, qryfasta->seq.s, &n_reg, tbuf, &mopt, 0); // get all hits for the query
        

         if (DO_PRINTF) printf ("# ## ALIGNMENTS: %s in [%s]\n",qryfasta->name.s, path);
			if (n_reg != 0) { 
            for (j = 0; j < n_reg; ++j) { // traverse hits and print them out
              mm_reg1_t *r = &reg[j];
              edit_distance = r->blen - r->mlen + r->p->n_ambi;
              assert(r->p); // with MM_F_CIGAR, this should not be NULL
              alignment_score = r->p->dp_score;
              if (edit_distance >= 0) {
                  hits++;
                 char *paf = 0;
                 int max_buf_len = 0; 
                 current_global_hitid++;
                 mm_gen_cs(0, &paf, &max_buf_len, mi, r, qryfasta->seq.s, 0);  // this generates the paf string
                 if (DO_PRINTF) printf("hitnum: %d hitid=%ld:mm2scr=%d\n", hits, current_global_hitid, alignment_score);
                 if (DO_PRINTF) printf("   refhit: %s\n", mi->seq[r->rid].name);
                 if (!str_equals(mi->seq[r->rid].name, last_contig_hit_name)) {
                    contigs++;
                    strncpy(last_contig_hit_name, mi->seq[r->rid].name, 64);
                 }

                 if (cs_globals->show_sampaf) {      // SAMPAF INFO
                    if (DO_PRINTF) printf("   sampaf: ");
                    if (DO_PRINTF) printf("%s\t%zu\t%d\t%d\t%c\t", qryfasta->name.s, qryfasta->seq.l, r->qs, r->qe, "+-"[r->rev]);
                    if (DO_PRINTF) printf("%s\t%d\t%d\t%d\t%d\t%d\t%d\tnm:i:%d\tcg:Z:", mi->seq[r->rid].name, mi->seq[r->rid].len, r->rs, r->re, r->mlen, r->blen, r->mapq, edit_distance);
                    if (DO_PRINTF) printf("%s\n", paf);
                 } 
                 // pthread_mutex_lock(&lock);
                 char *topstr = cs2mpas_entry_alloc (paf, 1);
                 char *botstr = cs2mpas_entry_alloc (paf, 0);
                 // pthread_mutex_unlock(&lock);

                 char *leader_space = NULL;
                 int leader_size = 0;
                 leader_size = r->qs;
                 leader_space = new_zero_str(r->qs + 1);
                 int i;
                 for (i=0;i<leader_size;i++) {
                    leader_space[i] = ' ';
                    leader_space[i+1] = 0;
                 }

                 if (r->rev) {
                  //   pthread_mutex_lock(&lock);
                    char *rc_qs = revcomp_alloc(qryfasta->seq.s);
                  //   pthread_mutex_unlock(&lock);
                    if (DO_PRINTF) printf("   _query: %s\n", rc_qs);
                    free_mem(rc_qs);
                 } else {
                     if (DO_PRINTF) printf("   _query: %s\n", qryfasta->seq.s);
                 }
                 if (DO_PRINTF) printf("   refseq:%s %s\n", leader_space, topstr);
                 if (DO_PRINTF) printf("   qedits:%s %s\n", leader_space, botstr);
                 
                 char *rcs = NULL;
                 if (cs_globals->show_rcigar) {      // RCIGAR INFO
                    if (DO_PRINTF) printf("   rcigar: ");
                    rcs = rcigar_alloc (topstr, botstr);
                    if (!r->rev) {         // oddly, the r->rev needs to be flipped
                       if (DO_PRINTF) printf("--> %s -->\n", rcs);
                    } else {
                       if (DO_PRINTF) printf("<-- %s <--\n", rcs);
                    }
                 }
                 free_mem(rcs);
                 
                 char hitid_str[HITID_STR_MAX];
                 hitid_str[0] = 0;
                 snprintf(hitid_str, HITID_STR_MAX, "%-38s, hit_num_%-6zu", qryfasta->name.s, current_global_hitid);
                 add_alignment (leader_space, botstr, edit_distance, hitid_str, r->rs, r->re, path);
                 
                 if (cs_globals->show_bed) {         // BED INFO
                    printf("   bedroi: ");
                    char *bedstr = bed_line_alloc (mi->seq[r->rid].name, r->rs, r->re, current_global_hitid, (double) alignment_score, "+-"[r->rev]);
                    if (DO_PRINTF) printf("%s\n",bedstr);
                    free_mem(bedstr);
                 }
                 hit_counter++;
                 edsum += edit_distance;
                 free_mem(paf);
                 free_mem(topstr);
                 free_mem(botstr);
              } else {
                 printf("   nohits: ed %d > max_edit_distance value %d", edit_distance, cs_globals->max_edit_distance);
              }
              free(r->p);
           }
           free(reg);
           
         float cons_scr = 1.0f;
         float fraction = 1.0f;
         if (hits == 0) 
            cons_scr = 1.0f;
         else {
            if (hits != 0) 
               fraction = 1.0 / hits;
               edave = (float) edsum / (float) hits;
            }
            if (edave == 0.0)
               cons_scr = 1.0f;
            else
               cons_scr = 1.0f - (fraction * edave);
           if (DO_PRINTF) printf("# ## CONSERVATION_CODE: copies=%d:contigs=%d:edave=%f:csvscr=%f\n\n", hits, contigs, edave, cons_scr);
         } else {
            printf("nohits: No Hits observed\n");
            free(reg);
            continue;
			}
		}
		mm_tbuf_destroy(tbuf);
		mm_idx_destroy(mi);
	}

   if (cs_globals->flush) fflush (stdout);   // done on the block so as to make sure a block is not broken up
	mm_idx_reader_close(r); // close the index reader
   if (str_equals(instruction, CMD_VALUE_CURRENT_REF))
      if (cs_globals->extract_mode == extract_mode_alignment_extra || cs_globals->extract_mode == extract_mode_extra) {
	      unload_seq_file(CMD_VALUE_CURRENT_REF);
      }
	kseq_destroy(qryfasta); // close the query file
	gzclose(roif);
	return 1;
}
void show_help (int arg_cnt, char **this_argv) {
   int print_help = 0;

   if (arg_cnt == 0) {
      print_help = 1;
   } else {
      if (arg_cnt == 1)  {
         if (!strstr(this_argv[1], ".cstp")) {
            print_help = 1;
         } else {
            if (arg_cnt < 7) 
               print_help = 1;
         }
      }
   }
   if (print_help) {
      printf ("Usage: %s <instructions.cstp>\n", this_argv[0]);
      printf ("   or \n");
      printf ("Usage: %s <ROI genome> <ROI lo> <ROI hi> <repeats file> <CDS file> <reference genome 1> .. <reference genome n>\n", this_argv[0]);
      printf ("\t%s is designed to extract a region of interest from the ROI genome and\n", this_argv[0]);
      printf ("\tthen align that region to a repeat database, a coding sequence database and then\n");
      printf ("\tall of the reference genomes passed to it. \n");
      printf ("\t%s will then report on any hits to repeats/TE's, coding sequence and then\n", this_argv[0]);
      printf ("\tall hits to the other references passed to it. It will also report on all\n");
      printf ("\tduplications. What is reported is controlled by the edit distance value. \n");
      printf ("\tWhen run using command line options, the edit distance is fixed at 10.\n");
      printf ("\tIf an instructions file is used, that value can be adjusted per reference alignment.\n");
      printf ("\tPlease review details by calling  'man composer_tools' on how to create an instructions file.\n");
      exit (0);
   }
}
int main (int argc, char *argv[]) {
   show_help(argc, argv);
   int arg_idx = 1;
	mm_verbose = 2; // disable message output to stderr
   cs_globals = (Globals *) malloc (sizeof(Globals));
   assert (cs_globals);
   setup();
   if (strstr(argv[1], ".cstp")) {  // a cstp file has all the step commands in it
      // open the file and pass, step by step 
      step_through_file(argv[1]);
   } else if (strstr (argv[1], ".fa")) {
      char *instruction = new_zero_str(_PATH_MAX);
      sprintf(instruction, "%s=%s", key_query_file , argv[1]);
      run_step(instruction);
      for(arg_idx = 2;arg_idx < argc;arg_idx++) {
         sprintf(instruction, "%s=%s", key_ref_file, argv[arg_idx]);       
            run_step(instruction);
         sprintf(instruction, "%s=%s", key_mode, analysis_mode_hapblock);  
            run_step(instruction);
         sprintf(instruction, "!%s:%s", CMD_ALIGN,CMD_VALUE_CURRENT_REF);  
            run_step(instruction);
         print_progress(instruction);
       }
      free_mem(instruction);
   } else {
      char *instruction = new_zero_str(_PATH_MAX);
      sprintf(instruction, "%s=%s", key_roi_ref_file, argv[arg_idx++]);    
         run_step(instruction);
      sprintf(instruction, "%s=%s", key_roi_seqid, argv[arg_idx++]);       
         run_step(instruction);
      sprintf(instruction, "%s=%s", key_roi_lo, argv[arg_idx++]);          
         run_step(instruction);
      sprintf(instruction, "%s=%s", key_roi_hi, argv[arg_idx++]);          
         run_step(instruction);
      sprintf(instruction, "%s=%s", key_repel_file, argv[arg_idx++]);      
         run_step(instruction);
      sprintf(instruction, "%s=%s", key_cds_file, argv[arg_idx++]);        
         run_step(instruction);
      print_progress(instruction);
      sprintf(instruction, "!%s:%s", CMD_SAVE,CMD_VALUE_ROI);              
         run_step(instruction);
      sprintf(instruction, "!%s:%s", CMD_ALIGN,CMD_VALUE_REPEL);           
         run_step(instruction);
      print_progress(instruction);
      sprintf(instruction, "!%s:%s", CMD_ALIGN,CMD_VALUE_CDS);             
         run_step(instruction);
      print_progress(instruction);
      
      for(;arg_idx < argc;arg_idx++) {
         sprintf(instruction, "%s=%s", key_ref_file, argv[arg_idx]);       
            run_step(instruction);
         sprintf(instruction, "%s=%s", key_mode, analysis_mode_hapblock);  
            run_step(instruction);
         sprintf(instruction, "!%s:%s", CMD_ALIGN,CMD_VALUE_CURRENT_REF);  
            run_step(instruction);
         print_progress(instruction);
       }

      free_mem(instruction);
   }
   print_alignments();
   breakdown();
   return 0;
}  
